## HGGLDial Dial Widget For Android. (API 8+). ##

**This dial widget is an advanced rotation control that you can easily include into any Android project in the form of an AAR file (Androids’ equivalent of a JAR file). You can think of this Dial control as a gesture library that is just concerned with rotation; having all of the conceivable behaviour you could possibly want in a rotation control.
**

### Features of the HGGLDial Library: ###

1. Powered by OpenGLES 2.0 this is a very powerful and responsive library. 
1. Simple classic Android usage, modeled on classic Android programming patterns. 
1. A dynamic return type, returned in a callback containing all the status information you will need for any programming scenarios. 
1. Open ended architecture to achieve maximum usage scenarios. 
1. Dial objects have the ability to act upon other dial objects, ie interacting with one dial can cause rotation in another dial at various rates of rotation (see Clock Demo in the demo app). 
1. Feature to distort a dial and rotate along the distortion (see Distortion Demo in the demo app). 
1. Advanced rotate snapping behavior with advanced proximity settings (ability to rotate freely for a certain amount of degrees before snap event occurs). 
1. Sensitivity settings. You can make the dial rotate slower or faster than the gesture or even in the opposite direction of the gesture. 
1. Single finger or dual finger mode. 
1. Cumulative and non cumulative rotation behavior. The dial can rotate directly to where the gesture starts or only rotate when the gesture moves; moving relative to the touch motion. 
1. The control keeps track of: The number of circular gestures, the number of image rotations, the direction of rotation, the XY positions of the touches from the gesture and the angle at which the touch is occurring. 
1. Minimum/Maximum rotation constraints. You can set the dial control to stop rotating at a certain number of rotations (clockwise or counter-clockwise). 
1. Alpha blending (see Clock Demo in the demo app). 
1. It is possible to adjust the center point of rotation for any image. 
1. It is possible to change the touch XY origin allowing you to rotate from a different position. 
1. It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the center of the dial.
1. Added a new fully configurable 'fling-to-spin' behaviour.
1. Last but not least: It’s open source and FREE to use. 

**Notice: HGDialV2 has landed check it out: https://bitbucket.org/warwick/hg_dial_v2**

1. Enhancements include: The way one dial acts upon another (acting dials) is greatly improved, optimised and very intuitive to the developer.
1. The angle snap now functions intuitively with any angle snap angle (the angle no longer has to be evenly divisible by 1).
1. Overall major optimisations.
1. Better separation of concerns.
1. Minimised lines of code.
1. Added new usages (Can now add arrays of dial objects).
1. Added save/restore and flush state objects.
1. Can now interact with multiple dials at the same time.
1. Works fluidly with device rotation.

**View the Youtube demonstration here: https://youtu.be/x4Dl4EdO5bk**

**For your convenience the demo app is now published to the Google Play Store: https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HGGLDialDemo&hl=en_GB**

**You can make a donation the the HG Widgets for Android here:**

**https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8**

Class Usages: (You will also find all of the necessary information in the developer docs included in this repository)
Usage 1. (For single textures in the GLView retrieved from the drawable attribute in the XML layout)


```
#!java

HGGLDial hgglDialDistort = (HGGLDial) rootView.findViewById(R.id.hgGLDistortionView);
```


The line of code below applies to both usage scenarios.


```
#!java

hgglDialDistort.registerCallback(new HGGLDial.IHGRotate() {
	@Override
	public void onDown(HGDialInfo hgDialInfo) {}
	@Override
	public void onPointerDown(HGDialInfo hgDialInfo) {}
	@Override
	public void onMove(HGDialInfo hgDialInfo) {}
	@Override
	public void onPointerUp(HGDialInfo hgDialInfo) {}
	@Override
	public void onUp(HGDialInfo hgDialInfo) {}
});
```


Usage 2. (For multiple textures in the GLView added dynamically as an array of Bitmaps)


```
#!java

MultipleDials[] multipleDials;
multipleDials = new MultipleDials[] {
		new MultipleDials(),
		new MultipleDials(),
		new MultipleDials()
};
multipleDials[0].setBitmap(bitmapHour);//The image to use for the dial
multipleDials[0].setPrecision(1);//Set precicion of each dial acting upon another dial
multipleDials[0].setOffset(new Point(0, 0));//The positions of the associated dial withing the GLView (Not Yet Tested)
multipleDials[0].setRapidDialAngle(0f);//Used for rapid dial functionality (see docs)

```
//.... set subscript 1 and 2 as above


```
#!java

HGRender hgRender = new HGRender(getResources(), multipleDials);
HGGLDial hggldialtime = (HGGLDial) rootView.findViewById(R.id.hgGLHourView);
hggldialtime.setRenderer(hgRender);
hgglDialDistort.registerCallback(HGGLDial.IHGRotate)//Call this line as in usage scenario 1.

```

XML Layout For both usages: See note below about two XML attributes not needed in usage scenario 2.


```
#!xml

<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.WarwickWestonWright.HGGLDialDemo.DistortionFragment">

	<FrameLayout
		android:layout_width="match_parent"
		android:layout_height="match_parent">

		<warwickwestonwright.hggldial.HGGLDial
			android:id="@+id/hgGLDistortionView"
			android:layout_width="match_parent"
			android:layout_height="match_parent"
			app:hg_drawable="@drawable/distortion_dial"

	</FrameLayout>

</RelativeLayout>
```


Note: the following two lines are only needed for usage scenario 1. Remove these two lines for usage scenario 2.
xmlns:app="http://schemas.android.com/apk/res-auto" (Not needed in usage scenario 2)
app:hg_drawable="@drawable/distortion_dial" (Not needed in usage scenario 2)

Alternatively you can implement 'HGGLDial.IHGRotate' in your class and call: hgglDialDistort.registerCallback(this);

### What could it be used for with all of these features? ###

**It could be used for making an analogue clock for reading and/or setting the time, It could be used as a volume control in your music or video app, it could be used as an alternative way of selecting numbers, it could be used to make a simulation of an old fashioned telephone dial, it could be used as an alternative star rating selector. Have an app that controls things remotely like a thermostat, you could use it for that, you could use it as a custom progress dialog. The usages are only limited by your imagination.
**

Any bugs please report to warwickwestonwright@gmail.com. You may also use this email if you want to commission my services. The projects in this repo are all in Android Studio format.

**Developer Notice:**

HGDialV2 is currently in development. Once completed the developer will continue development HacerGestoV3. The architecture of HacerGestoV3 will be based on HGDialV2. HGDialV2 will have improvements on how one dial acts upon each other as this was very poor and un-intuitive in HGDial V1. HGDial V1 will remain available after V2 is complete. The developer may be further optimisations for HGDial V1 depending on what happens with HGDial V2.

**LICENSE.**

This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2016, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the

distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.