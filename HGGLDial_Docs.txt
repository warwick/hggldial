HGGLDial Developer Documentation.
Overview:
Structure of this document.
This document is divided into 8 parts.

1	Overview.
2	Features of the HGGLDial Library.
3	What could it be used for with all of these features?
4	Key Classes in this library.
5	Class Usages: Constructing the object and setting up the XML layouts.
6	Methods with their descriptions, usage and appropriate advisory notes.
7	Additional Class Information.

Overview:
This dial widget is an advanced rotation control that you can easily include into any Android project in the form of an AAR file (Androids� equivalent of a JAR file). You can think of this Dial control as a gesture library that is just concerned with rotation; having all of the conceivable behaviour you could possibly want in a rotation control.

Features of the HGGLDial Library:
The Features include:
1.	Powered by OpenGLES 2.0 this is a very powerful and responsive library.
2.	Simple classic Android usage, modelled on classic Android programming patterns.
3.	A dynamic return type, returned in a callback containing all the status information you will need for any programming scenarios.
4.	Open ended architecture to achieve maximum usage scenarios.
5.	Dial objects have the ability to act upon other dial objects, ie interacting with one dial can cause rotation in another dial at various rates of rotation (see Clock Demo in the demo app).
6.	Feature to distort a dial and rotate along the distortion (see Distortion Demo in the demo app).
7.	Advanced rotate snapping behaviour with advanced proximity settings (ability to rotate freely for a certain amount of degrees before snap event occurs).
8.	Sensitivity settings. You can make the dial rotate slower or faster than the gesture or even in the opposite direction of the gesture.
9.	Single finger or dual finger mode.
10.	Cumulative and non cumulative rotation behaviour. The dial can rotate directly to where the gesture starts or only rotate when the gesture moves; moving relative to the touch motion.
11.	The control keeps track of: The number of circular gestures, the number of image rotations, the direction of rotation, the XY positions of the touches from the gesture and the angle at which the touch is occurring.
12.	Minimum/Maximum rotation constraints. You can set the dial control to stop rotating at a certain number of rotations (clockwise or counter-clockwise).
13.	Alpha blending (see Clock Demo in the demo app).
14.	It is possible to adjust the centre point of rotation for any image.
15.	It is possible to change the touch XY origin allowing you to rotate from a different position.
16.	It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the centre of the dial.
17.	This lib also has a fully configurable fling-to-spin behaviour.
18.	Last but not least: It�s open source and FREE to use.

What could it be used for with all of these features?

It could be used for making an analogue clock for reading and/or setting the time, It could be used as a volume control in your music or video app, it could be used as an alternative way of selecting numbers, it could be used to make a simulation of an old fashioned telephone dial, it could be used as an alternative star rating selector. Have an app that controls things remotely like a thermostat, you could use it as a custom progress dialog. The usages are only limited by your imagination. 
Key Classes in this library:
HGGLDialInfo: Contains the state/information returned to the developer in the callback. This status dynamically changes in response to touch events.
HGGLGeometry: Contains some very useful geometry functions for your convenience. This class will progressively have more methods added as the library evolves.
HGGLDial: The main library class.
HGGLRender: The standard OpenGLES render class (Uses OpenGLES v2.0)
ImageUtilities: Contains image manipulation functions. This class will progressively have more methods added as the library evolves.
MultipleDials: Class that must be instantiated and passed to constructor of the Render class. This only applies to usage scenario 2. (See Class Usages below).
 Class Usages:
Usage 1:
HGGLDial hgglDial = (HGGLDial) rootView.findViewById(R.id.hgGLDemoView)

XML Layout associated with Usage 1.
<RelativeLayout
	android:layout_width="wrap_content"
	android:layout_height="wrap_content"
android:layout_centerHorizontal="true">
	<warwickwestonwright.hggldial.HGGLDial
		android:id="@+id/hgGLDemoView"
		android:layout_width="match_parent"
		android:layout_height="wrap_content"
		app:hg_drawable="@drawable/demo_dial"/>
</RelativeLayout>
Usage 2:
MultipleDials[] multipleDials = new MultipleDials[] {
new MultipleDials(),
new MultipleDials()
};

multipleDials[0].setBitmap(bitmapHour);//Image for the texture
multipleDials[0].setPrecision(1);//Precision of the texture
multipleDials[0].setOffset(new Point(0, 0));//Rotation origin
multipleDials[1].setBitmap(bitmapHour);//Image for the texture
multipleDials[1].setPrecision(2);//Precision of the texture
multipleDials[1].setOffset(new Point(0, 0));//Rotation origin
HGGLRender hgglRender = new HGGLRender(getResources(), multipleDials);
HGGLDial hggldialtime = (HGGLDial) rootView.findViewById(R.id.hgGLHourView);
hggldialtime.setRenderer(hgglRender);

XML Layout for usage 2
<RelativeLayout
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	tools:context="com.WarwickWestonWright.HGGLDialDemo.HourFragment">

	<warwickwestonwright.hggldial.HGGLDial
		android:id="@+id/hgGLHourView"
		android:layout_width="wrap_content"
		android:layout_height="wrap_content"/>

</RelativeLayout>
 
After constructing the necessary objects (for both usages) you need to implement the callback by calling registerCall-back on the HGGLDial instance as follows:
hgglDial.registerCallback(new HGGLDial.IHGRotate() {
	@Override
	public void onDown(HGGLDialInfo hgglDialInfo) {}
	@Override
	public void onPointerDown(HGGLDialInfo hgglDialInfo) {}
	@Override
	public void onMove(HGGLDialInfo hgglDialInfo) {}
	@Override
	public void onPointerUp(HGGLDialInfo hgglDialInfo) {}
	@Override
public void onUp(HGGLDialInfo hgglDialInfo) {}
});

Note: As an additional classic usage you can implement HGGLDial.IHGRotate in your class and then call hgglDial.registerCallback(this);
Note: The HGGLDialInfo object returned by the callback contains the status of the touch event. The methods of this class that you need to be concerned with are as follows:
boolean getQuickTap()
Triggered when a quick tap event occurs

int getGestureRotationDirection()//Concerned with the gesture not the texture
Returns -1 for counter clockwise and 1 for clock wise and 0 when settled.

int getObjectRotationDirection()//Concerned with the texture not the gesture
Returns -1 for counter clockwise and 1 for clock wise and 0 when settled.

Double getGestureAngleBaseOne()
Retrieves the angle of the gesture.
Double getObjectAngleBaseOne()
Retrieves the angle of the texture.

float getFirstTouchX()//Returns touch positions.
float getFirstTouchY()//Returns touch positions.
float getSecondTouchX()//Returns touch positions.
float getSecondTouchY()//Returns touch positions.

int getObjectRotationCount()
Gets the texture rotation count.

int getGestureRotationCount()
Gets the gesture rotation count.

Double getGestureTouchAngle()
Gets the angle of the touch relative to the centre point.

boolean getRotateSnapped()
Triggered to return true when an able snap occurs. Note: Allways returns true if there is no free rotation between snaps.

Double getVariablePrecision()
Designed to get the current precision when using variable dial behaviour, but will also work when not using variable dial.

int getFlingDistanceThreshold()
Used to get the fling threshold set in a call to setFlingTolerance.

long getFlingTimeThreshold()
Used to get the time threshold set in a call to setFlingTolerance.
float getSpinStartSpeed()
Used to get the spin start speed (in rotations per second) set in a call to setSpinAnimation.

float getSpinEndSpeed()
Used to get the spin end speed (in rotations per second) set in a call to setSpinAnimation.

long getSpinDuration()
Used to get the spin animation length (in milliseconds) set in a call to setSpinAnimation.

int getGestureCountEvent()//Triggered when a full rotation occurs.
int getObjectCountEvent()//Triggered when a full rotation occurs.
int getMinMaxRotationOutOfBounds()
Returns value indicating when rotation boundary has been met. 1 for maximum boundary met, -1 for minimum boundary met or 0 when rotation count is within rotation bounds. (Only for use when using min max bounds behaviour).
Note: This class has public mutators with relative naming conventions. They are only public because they are used internally by another class and you need not be concerned with them, but by all means experiment with the behaviour of calling them.

Methods with their descriptions, usage and appropriate advisory notes. 
void registerCallback(HGGLDial.IHGGLDial) Important!
Main method to register call back. The library will not function without this. If you have implemented HGGLDial.IHGGLDial in your Acivity or Fragment then call this method with parsing the �this� object

void setQuickTapTime(final long quickTapTime)
Sets the quick tap sensitivity in milliseconds.

void setCumulativeRotate(final boolean cumulativeRotate)
Setter for cumulative rotate behaviour.
 
void setIsSingleFinger(final boolean isSingleFinger)
Sets the behaviour for single finger or dual finger operation.

void setPrecisionRotation(final double precisionRotation)
Setter to dictage how many times the texture rotates in response to the gesture.

void setAngleSnapBaseOne(final double angleSnapBaseOne, final double angleSnapProximity)
Enables angle snapping. First parameter is for the angle to snap to (expressed in rations of 0 - 1), second parameter dictates the distance of free rotation before a snap event occurs. Calling it with values of 0.125 and 0.03125 would cause the texture to freely rotate for 22.5 degrees before snapping into a 45 degree position.

void doRapidDial(final MultipleDials[] multipleDials)
calling this arbitrarily dials the texture to a given angle; ignoring all other functionality of the library. This method is designed to be used with multiple textures, such as in usage two. (See code in the clock demo of the demo app)

void setTouchOffset(final int touchOffsetX, final int touchOffsetY)
Calling this method allows the textures to be manipulated from positions other than the centre point.

static void setCenterPoint(final Point centerPoint)
Used internally by the Renderer class. Unsafe to call directly

static void setMultipleDials(MultipleDials[] multipleDials)
Used internally by the Renderer class. Unsafe to call directly

void setDistortDialX(float distortDialX) and void setDistortDialY(float distortDialY)
Used to set distorted dial behaviour.

void setSuppressRender(final boolean suppressRender)
Parsing true to this method will cause the texture to stop animating, but all other calculations will still occur normally.
void setVariableDial(final double variableDialInner, final double variableDialOuter, final boolean useVariableDial)
Calling this method will cause the dial to rotate at different speeds depending on how close the touch is to the centre of the dial. Calling this method with 4.5, 0.8, true; would cause the dial to rotate 4.5 times for every circular gesture when dialling close to the centre and 0.8 times when dialling near the out edge. The third parameter simple enables or disables the behaviour.

void setUseVariableDialCurve(boolean useVariableDialCurve, boolean positiveCurve)
Calling this method will cause the variable dial acceleration/deceleration to work on a curve. The first parameter tells the library to use the behaviour; setting positiveCurve to true will cause the dial to accelerate towards the outer dial and false will cause deceleration towards the outer dial.  This behaviour was added to make it easier to zero in to accurate angles when the inner precision is very high.
void setMinMaxDial(final double minimumRotation, final double maximumRotation, boolean useMinMaxRotation)

Sets the minimum/maximum rotation constraints. Last parameter simple enables/disables the rotation constraints.

void setFlingTolerance(int flingDistanceThreshold, long flingTimeThreshold)
Used to set the fling-to-spin behaviour. The two parameters set the fling tolerance. When a fling event occurs a spin animation will occur. See method below setSpinAnimation. The first parameter is expressed in pixels and the time is in milliseconds.

void setSpinAnimation(float spinStartSpeed, float spinEndSpeed, long spinDuration)
Used to set the start/end speed (expressed in rotations per second) and duration of the spin animation (in milliseconds) triggered by a fling event. See method above setFlingTolerance. Note: Setting the first two parameters to 0 and the duration to non zero will cause the start spin speed to be relative to the speed of the fling.

Point getPointFromAngle(final float angle)
Convenience method to get a point from a given angle; assuming the radius is the radius of the texture.

void doManualGestureDial(final double manualDial)
Convenience method to move the dial to a given angle and rotation count.
void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection)
Convenience method, to fire a fling-to-spin event. The start and end speeds are expressed in rotations per second and the spin duration is in milliseconds. Pass a direction of -1 for the spin animation to rotate counter clockwise and 1 for clockwise.

void doManualObjectDial(final double manualDial)
Convenience method for rotating the gesture to a given value.

void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView)
Used internally by the Renderer Class. Unsafe to call.

void performQuickTap()
Convenience method to programmatically invoke a quick tap event.

boolean onTouchEvent(MotionEvent event)
Convenience method to retrieve the internal motion event from the dial object.

void sendTouchEvent(View v, MotionEvent event)
Convenience method to inject a touch event into the dial object from another view.

long getQuickTapTime()
Gets the quick tap tolerance in millisectonds.

boolean getCumulativeRotate()
Retrieves the flag for cumulative dial behaviour.

boolean getIsSingleFinger()
Gets the mode of operation ie single or dual touch.

Double getPrecisionRotation()
Gets the value for the cumulative rotation behaviour.
 
Double getAngleSnapBaseOne()
Gets the angle for angle snapping behaviour (set in the first parameter of the mutator)

Double getAngleSnapProximity()
Gets the proximity for the angle snapping behaviour(set in the second parameter of the mutator)

OnTouchListener retrieveLocalOnTouchListener()
Convenience method to retrieve the internal touch even listener.

float getTouchOffsetX() and float getTouchOffsetY()
Gets the X or Y value of the touch offset. (The library can be programmed to manipulate textures from a location other than the centre point)

static Point getCenterPoint()
Gets the centre point of the view port.

float getDistortDialX() and float getDistortDialY()
Gets the scale distortion for distorted dial behaviour.

boolean hasAngleSnapped()
Returns true when an agle snap even occurs. You can also retrieve this value from the getRotateSnapped method of the callback object.

boolean getSuppressRender()
Returns true when the dial has been set to not render from a call to setSuppressRender

Double getVariableDialInner(),double getVariableDialOuter() and boolean getUseVariableDial()
Gets the inner/outer dial precision for variable dial behaviour; along with the flag to detect if variable dial behaviour is being used.
 
boolean getUseVariableDialCurve()
Retrieves the value set in the first parameter from a call to the setUseVariableDialCurve method.

boolean getPositiveCurve()
Retrieves the value set in the second parameter from a call to the setUseVariableDialCurve method.

Double getGestureFullAngle()
Convenience method to retrieve the full angle of the gesture.

int getGestureCountEvent()
Returns a value to detect when a full gesture rotation has occurred. Retruns -1 for counter clockwise, 1 for clockwise and zero when no rotation bounds have been met.

int getObjectCountEvent()
Returns a value to detect when a full texture rotation has occurred. Retruns -1 for counter clockwise, 1 for clockwise and zero when no rotation bounds have been met.

Additional Class Information.
The Image Utilities Class

At present this class only has one static method:

static Bitmap getProportionalBitmap(final Bitmap bitmap, int newDimensionXorY, String XorY)
Returns a proportionally scaled bitmap. This is used internally by the HGGLRender class but is available for your convenience. The second parameter is the new height or width the last parameter indicates what dimension you are sending.

The HGGLRender Class
This class has two overloaded constructors.
 
HGGLRender(Resources resources, int imageResourceId)
This constructor is for usage scenario 2 when using a single texture. (NEW)

HGGLRender(Resources resources, MultipleDials[] multipleDials)
This constructor is used for usage scenario 2 when you need to use multiple textures, but will also work for a single texture.

static void registerRenderer(IHGRender ihgRender)
Used Internally, unsafe to call publically.

static void setAdjustViewBounds(boolean adjustViewBounds)
Used to facilitate scaling when using distorted dial behaviour (See Demo app).

static boolean getAdjustViewBounds()
Accessor for the above mutator.

static MultipleDials[] getMultipleDials()
Used to retrieve settings for multiple textures

static void setUseRapidDial(boolean useRapidDial)
Used Internally by the HGGLRender class. Unsafe to call publically.

static boolean getUseRapidDial()
Used Internally. 

static int[] getOriginalHeightAndWidth()
Gets the view port dimensions.
 
static Point getCenterPoint()
Used Internally Can be used to get measurements during instantiation cycles.

static Point getCenterPoint()
Method to get the centre point for the view port.

void onSurfaceCreated(GL10 unused, EGLConfig config)
Used Internally. Unsage to call directly.

void onDrawFrame(GL10 unused)
Used Internally. Unsage to call directly.

The HGGeometry Class
static float getAngleFromPoint(final Point centerPoint, final Point touchPoint)
Does what is sais on the box.

static Point getPointFromAngle(final float angle, final float radius)
Does what is sais on the box.

double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)
Calculates the diagonal distance between two points.

static float[] pythagoreanTheorem(final float diagonalLength, final float width, float height)
Gets the width and height from given diagonal length and width to height ratio. 

The GetAngleWrapper class:
This is being deprecated. Any flags needed here will be migrated to the HGGLDial class.
