/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements MainFragment.IMainFragment {

    private MainFragment mainFragment;
    private DistortionFragment distortionFragment;
    private ClockFragment clockFragment;
    private DemoFragment demoFragment;
    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        getSupportActionBar().hide();

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        if(sp.getBoolean("spIsSetup", false) == false) {

            //Initialise Rotate settings.
            sp.edit().putBoolean("cumulativeRotate", true).commit();
            sp.edit().putBoolean("isSingleFinger", true).commit();
            sp.edit().putFloat("precisionRotation", 0.5f).commit();
			sp.edit().putBoolean("precisionRotationEnable", false).commit();
            sp.edit().putFloat("angleSnapBaseOne", 0.125f).commit();
            sp.edit().putFloat("angleSnapProximity", 0.03125f).commit();
            sp.edit().putFloat("minimumRotation", -2.45f).commit();
            sp.edit().putFloat("maximumRotation", 3.2f).commit();
            sp.edit().putBoolean("useAngleSnap", false).commit();
            sp.edit().putBoolean("useMinMaxRotate", false).commit();
			sp.edit().putBoolean("useVariableDial", false).commit();
			sp.edit().putBoolean("useVariableDialCurve", false).commit();
			sp.edit().putBoolean("positiveCurve", false).commit();
            sp.edit().putFloat("variableDialInner", 4.5f).commit();
            sp.edit().putFloat("variableDialOuter", 0.8f).commit();
            sp.edit().putBoolean("enableFling", false).commit();
			sp.edit().putInt("flingDistance", 200).commit();
			sp.edit().putLong("flingTime", 250l).commit();
			sp.edit().putInt("startSpeed", 10).commit();
			sp.edit().putInt("endSpeed", 0).commit();
			sp.edit().putLong("spinAnimationTime", 5000l).commit();

            sp.edit().putBoolean("spIsSetup", true).commit();

        }//End if(sharedPreferences.getBoolean("spIsSetup", false) == false)

        if(savedInstanceState == null) {

            if(mainFragment == null) {

                mainFragment = new MainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

            }

        }//End if(savedInstanceState == null)

    }//End protected void onCreate(Bundle savedInstanceState)


    @Override
    public void onBackPressed() {

        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag("MainFragment");

        if(mainFragment != null && mainFragment.isAdded() == true) {

            super.onBackPressed();

        }
        else {

            if(mainFragment == null) {

                mainFragment = new MainFragment();

            }

            getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, mainFragment, "MainFragment").commit();

        }//End if(mainFragment != null && mainFragment.isAdded() == true)

    }//End public void onBackPressed()


    @Override
    public void mainFragmentCallback(String fragmentName) {

        if(fragmentName == DistortionFragment.class.getName()) {

            if(distortionFragment == null) {distortionFragment = new DistortionFragment();}
            getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, distortionFragment, "DistortionFragment").commit();

        }
        else if(fragmentName == ClockFragment.class.getName()) {

            if(clockFragment == null) {clockFragment = new ClockFragment();}
            getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, clockFragment, "ClockFragment").commit();

        }
        else if(fragmentName == DemoFragment.class.getName()) {

            if(demoFragment == null) {demoFragment = new DemoFragment();}
            getSupportFragmentManager().beginTransaction().replace(R.id.mainActivityContainer, demoFragment, "DemoFragment").commit();

        }

    }//End public void mainFragmentCallback(String fragmentName)

}