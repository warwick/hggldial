/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import warwickwestonwright.hggldial.HGGLDial;
import warwickwestonwright.hggldial.HGGLDialInfo;
import warwickwestonwright.hggldial.ImageUtilities;

public class DemoFragment extends Fragment implements DialogInterface.OnClickListener {

    public DemoFragment() {}

    private MainSettingsFragment mainSettingsFragment;
    private SharedPreferences sp;
    private TextView lblHGGLDialInfo;
    private HGGLDial hgglDial;
    private Bitmap bitmap;
    private AssetManager assetManager;
    private InputStream assetStream;
    private static Point point;
    private View rootView;
    private static String dialInfo = "Rotation Status Will Be Displayed Here!\nTAP DIAL TO BRING UP SETTINGS\n\n\n\n\n\n";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.demo_fragment, container, false);
        lblHGGLDialInfo = (TextView) rootView.findViewById(R.id.lblHGGLDialInfo);
        lblHGGLDialInfo.setText(dialInfo);

        sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        assetManager = getActivity().getAssets();

        try {

            assetStream = assetManager.open("demo_dial.png");
            bitmap = BitmapFactory.decodeStream(assetStream);
            bitmap = ImageUtilities.getProportionalBitmap(bitmap, (int) (point.x / 1.2f), "X");

        }
        catch(IOException e) {

            e.printStackTrace();

        }

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    private void setupDemoFragmentView() {

        hgglDial = (HGGLDial) rootView.findViewById(R.id.hgGLDemoView);

        hgglDial.registerCallback(new HGGLDial.IHGRotate() {
            @Override
            public void onDown(HGGLDialInfo hgglDialInfo) {

                dialInfo = "X: " + Float.toString(hgglDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgglDialInfo.getFirstTouchY()) +
                        "\nGesture Angle: " + Double.toString(hgglDialInfo.getGestureAngleBaseOne()) +
                        "\nObject Angle: " + Double.toString(hgglDialInfo.getObjectAngleBaseOne()) +
                        "\nGesture Direction: " + Integer.toString(hgglDialInfo.getGestureRotationDirection()) +
                        "\nObject Direction: " + Integer.toString(hgglDialInfo.getObjectRotationDirection()) +
                        "\nGesture Rotation Count: " + Integer.toString(hgglDialInfo.getGestureRotationCount()) +
                        "\nObject Rotation Count: " + Integer.toString(hgglDialInfo.getObjectRotationCount()) +
                        "\nPrecision: " + Double.toString(hgglDialInfo.getVariablePrecision());
                lblHGGLDialInfo.setText(dialInfo);

            }

            @Override
            public void onPointerDown(HGGLDialInfo hgglDialInfo) {}

            @Override
            public void onMove(HGGLDialInfo hgglDialInfo) {

                //This information will be displayed and dynamically updated when the dial is touched for a full list of callback information see the HGGLDialInfo class
                dialInfo = "X: " + Float.toString(hgglDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgglDialInfo.getFirstTouchY()) +
                        "\nGesture Angle: " + Double.toString(hgglDialInfo.getGestureAngleBaseOne()) +
                        "\nObject Angle: " + Double.toString(hgglDialInfo.getObjectAngleBaseOne()) +
                        "\nGesture Direction: " + Integer.toString(hgglDialInfo.getGestureRotationDirection()) +
                        "\nObject Direction: " + Integer.toString(hgglDialInfo.getObjectRotationDirection()) +
                        "\nGesture Rotation Count: " + Integer.toString(hgglDialInfo.getGestureRotationCount()) +
                        "\nObject Rotation Count: " + Integer.toString(hgglDialInfo.getObjectRotationCount()) +
                        "\nPrecision: " + Double.toString(hgglDialInfo.getVariablePrecision());
                lblHGGLDialInfo.setText(dialInfo);

            }

            @Override
            public void onPointerUp(HGGLDialInfo hgglDialInfo) {}

            @Override
            public void onUp(HGGLDialInfo hgglDialInfo) {

                //This information will be displayed and dynamically updated when the dial is touched for a full list of callback information see the HGGLDialInfo class
                dialInfo = "X: " + Float.toString(hgglDialInfo.getFirstTouchX()) + ", Y: " + Float.toString(hgglDialInfo.getFirstTouchY()) +
                        "\nGesture Angle: " + Double.toString(hgglDialInfo.getGestureAngleBaseOne()) +
                        "\nObject Angle: " + Double.toString(hgglDialInfo.getObjectAngleBaseOne()) +
                        "\nGesture Direction: " + Integer.toString(hgglDialInfo.getGestureRotationDirection()) +
                        "\nObject Direction: " + Integer.toString(hgglDialInfo.getObjectRotationDirection()) +
                        "\nGesture Rotation Count: " + Integer.toString(hgglDialInfo.getGestureRotationCount()) +
                        "\nObject Rotation Count: " + Integer.toString(hgglDialInfo.getObjectRotationCount()) +
                        "\nPrecision: " + Double.toString(hgglDialInfo.getVariablePrecision());
                lblHGGLDialInfo.setText(dialInfo);

                if(hgglDialInfo.getQuickTap() == true) {

                    //Shows the settings dialog when you quick tap the dial.
                    //Top of block Show dial settings dialog so that user may change the settings for this dial object.
                    if(mainSettingsFragment == null) {

                        mainSettingsFragment = new MainSettingsFragment();

                    }
                    else if(mainSettingsFragment != null) {

                        mainSettingsFragment.dismiss();
                        getActivity().getSupportFragmentManager().beginTransaction().remove(mainSettingsFragment).commit();

                    }

                    if(mainSettingsFragment == null) {

                        mainSettingsFragment = new MainSettingsFragment();

                    }

                    mainSettingsFragment.setCancelable(true);
                    mainSettingsFragment.setTargetFragment(getActivity().getSupportFragmentManager().findFragmentByTag("DemoFragment"), 0);
                    mainSettingsFragment.show(getActivity().getSupportFragmentManager(), "MainSettingsFragment");
                    //Bottom of block Show dial settings dialog so that user may change the settings for this dial object.

                }//End if(hgResult.getQuickTap() == true)

            }
        });

    }//End private void setupDemoFragmentView()


    private void setUpPreferenceValues() {

        //This function is called when the view opens or when returning from the settings dialog.

        /* When cumulative rotate is false, the dial moves to the angle where the touch goes down and moves from there;
        set to true (default) the dial remains still when the touch goes down and only moves when the gesture moves */
        hgglDial.setCumulativeRotate(sp.getBoolean("cumulativeRotate", true));

        /* Gesture can act as a single finger or dual finger mode */
        hgglDial.setIsSingleFinger(sp.getBoolean("isSingleFinger", true));

        /* Sets the gesture sensitivity. When set to 1f (default) the image will rotate once for ever circular rotation you make with your finger
        when set to 2f the dial will rotate at twice the rate etc.. etc.. Note: it can be a negative number; causing the image/dial to rotate in the
        opposite direction of the gesture. */
        if(sp.getBoolean("precisionRotationEnable", false) == true) {

            hgglDial.setPrecisionRotation(sp.getFloat("precisionRotation", 1.0f));

        }
        else {

            hgglDial.setPrecisionRotation(1.0f);

        }

        if(sp.getBoolean("useAngleSnap", false) == true) {

            /* This method causes angle snapping behaviour. The first parameter is the angles to snap to. In this example the default setting will snap every
            45 degrees. The second parameter is the angle snap proximity. In this example it will rotate freely for 22.5 degrees before snapping. Note: The
            callback returns true when an angle is in a snap zone and false otherwise */
            hgglDial.setAngleSnapBaseOne(sp.getFloat("angleSnapBaseOne", 0.125f), sp.getFloat("angleSnapProximity", 0.03125f));

        }
        else {

            //Default angle snap vales (no snapping).
            hgglDial.setAngleSnapBaseOne(0, 0);

        }//End if(sharedPreferences.getBoolean("useAngleSnap", false) == true)

        if(sp.getBoolean("useMinMaxRotate", false) == true) {

            //This is a simple one. Just set the minimum number and maximum number and the dial will stop rotating when the angles are met.
            hgglDial.setMinMaxDial(sp.getFloat("minimumRotation", -2.45f), sp.getFloat("maximumRotation", 3.2f), sp.getBoolean("useMinMaxRotate", false));

        }
        else if(sp.getBoolean("useMinMaxRotate", false) == false) {

            //Default setting (no min/max rotation constraint)
            hgglDial.setMinMaxDial(0, 0, sp.getBoolean("useMinMaxRotate", false));

        }//End if(sharedPreferences.getBoolean("useMinMaxRotate", false) == true)

		/* Variable dial causes the dial to detect how close the touch is to the centre of the dial and dynamically adjust how many rotations the dial makes relative to the amount of circular gestures made. */
		hgglDial.setVariableDial(sp.getFloat("variableDialInner", 4.5f), sp.getFloat("variableDialOuter", 0.8f), sp.getBoolean("useVariableDial", false));

		if(sp.getBoolean("useVariableDialCurve", false) == true) {

			hgglDial.setUseVariableDialCurve(true, sp.getBoolean("positiveCurve", false));

		}
		else if(sp.getBoolean("useVariableDialCurve", false) == false) {

			hgglDial.setUseVariableDialCurve(false, sp.getBoolean("positiveCurve", false));

		}//End if(sharedPreferences.getBoolean("useVariableDialCurve", false) == true)

		if(sp.getBoolean("enableFling", false) == true) {

			hgglDial.setFlingTolerance(sp.getInt("flingDistance", 200), sp.getLong("flingTime", 250l));
			hgglDial.setSpinAnimation(sp.getInt("startSpeed", 10), sp.getInt("endSpeed", 0), sp.getLong("spinAnimationTime", 5000l));

		}
		else if(sp.getBoolean("enableFling", false) == false) {

			hgglDial.setFlingTolerance(0, 0l);
			hgglDial.setSpinAnimation(0, 0, 0l);

		}//End if(sp.getBoolean("enableFling", false) == true)

    }//End private void setUpPreferenceValues()


    @Override
    public void onPause() {
        super.onPause();

        //Boilerplate GLView code straight out of the Android docs
        hgglDial.onPause();

    }


    @Override
    public void onResume() {
        super.onResume();

        setupDemoFragmentView();
        setUpPreferenceValues();

        //Boilerplate GLView code straight out of the Android docs
        hgglDial.onResume();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    @Override
    public void onDetach() {
        super.onDetach();

		dialInfo = "Rotation Status Will Be Displayed Here!\nTAP DIAL TO BRING UP SETTINGS\n\n\n\n\n\n";

    }


    @Override
    public void onClick(DialogInterface dialog, int which) {

		if(dialog != null) {dialog.dismiss();}
        setUpPreferenceValues();

    }

}