/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainSettingsFragment extends DialogFragment implements  DialogInterface.OnDismissListener {

    private DialogInterface.OnClickListener onClickListener;
    public MainSettingsFragment() {}

    private View rootView;
    private CheckBox chkCumulativeEnable;
    private CheckBox chkSingleFingerEnable;
    private EditText txtPrecision;
	private CheckBox chkPrecisionEnable;
    private EditText txtAngleSnap;
    private EditText txtAngleSnapProximity;
    private EditText txtMinimumAngle;
    private EditText txtMaximumAngle;
    private CheckBox chkAngleSnapEnable;
    private CheckBox chkMinMaxEnable;
    private Button btnCloseAndDial;
    private CheckBox chkVariableDialEnable;
    private CheckBox chkUseVariableDialCurve;
    private CheckBox chkPositiveCurve;
    private EditText txtVariableDialInner;
    private EditText txtVariableDialOuter;
	private CheckBox chkEnableFling;
	private EditText txtFlingDistance;
	private EditText txtFlingTime;
	private EditText txtStartSpeed;
	private EditText txtEndSpeed;
	private EditText txtSpinAnimationTime;
    private SharedPreferences sp;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, getTheme());
        onClickListener = (DialogInterface.OnClickListener) getTargetFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.main_settings_fragment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		chkCumulativeEnable = (CheckBox) rootView.findViewById(R.id.chkCumulativeEnable);
		chkSingleFingerEnable = (CheckBox) rootView.findViewById(R.id.chkSingleFingerEnable);
		txtPrecision = (EditText) rootView.findViewById(R.id.txtPrecision);
		chkPrecisionEnable = (CheckBox) rootView.findViewById(R.id.chkPrecisionEnable);
		txtAngleSnap = (EditText) rootView.findViewById(R.id.txtAngleSnap);
        txtAngleSnapProximity = (EditText) rootView.findViewById(R.id.txtAngleSnapProximity);
		txtMinimumAngle = (EditText) rootView.findViewById(R.id.txtMinimumAngle);
		txtMaximumAngle = (EditText) rootView.findViewById(R.id.txtMaximumAngle);
		chkAngleSnapEnable = (CheckBox) rootView.findViewById(R.id.chkAngleSnapEnable);
		chkMinMaxEnable = (CheckBox) rootView.findViewById(R.id.chkMinMaxEnable);
		btnCloseAndDial = (Button) rootView.findViewById(R.id.btnCloseAndDial);
		chkVariableDialEnable = (CheckBox) rootView.findViewById(R.id.chkVariableDialEnable);
		chkUseVariableDialCurve = (CheckBox) rootView.findViewById(R.id.chkUseVariableDialCurve);
		chkPositiveCurve = (CheckBox) rootView.findViewById(R.id.chkPositiveCurve);
        txtVariableDialInner = (EditText) rootView.findViewById(R.id.txtVariableDialInner);
        txtVariableDialOuter = (EditText) rootView.findViewById(R.id.txtVariableDialOuter);
		chkEnableFling = (CheckBox) rootView.findViewById(R.id.chkEnableFling);
		txtFlingDistance = (EditText) rootView.findViewById(R.id.txtFlingDistance);
		txtFlingTime = (EditText) rootView.findViewById(R.id.txtFlingTime);
		txtStartSpeed = (EditText) rootView.findViewById(R.id.txtStartSpeed);
		txtEndSpeed = (EditText) rootView.findViewById(R.id.txtEndSpeed);
		txtSpinAnimationTime = (EditText) rootView.findViewById(R.id.txtSpinAnimationTime);

        chkCumulativeEnable.setChecked(sp.getBoolean("cumulativeRotate", true));
        chkSingleFingerEnable.setChecked(sp.getBoolean("isSingleFinger", true));
        txtPrecision.setText(Float.toString(sp.getFloat("precisionRotation", 1.0f)));
		chkPrecisionEnable.setChecked(sp.getBoolean("precisionRotationEnable", false));
        txtAngleSnap.setText(Float.toString(sp.getFloat("angleSnapBaseOne", 0.125f)));
        txtAngleSnapProximity.setText(Float.toString(sp.getFloat("angleSnapProximity", 0.03125f)));
        txtMinimumAngle.setText(Float.toString(sp.getFloat("minimumRotation", -2.45f)));
        txtMaximumAngle.setText(Float.toString(sp.getFloat("maximumRotation", 3.2f)));
        chkAngleSnapEnable.setChecked(sp.getBoolean("useAngleSnap", false));
        chkMinMaxEnable.setChecked(sp.getBoolean("useMinMaxRotate", false));
        chkVariableDialEnable.setChecked(sp.getBoolean("useVariableDial", false));
		chkUseVariableDialCurve.setChecked(sp.getBoolean("useVariableDialCurve", false));
		chkPositiveCurve.setChecked(sp.getBoolean("positiveCurve", false));
        txtVariableDialInner.setText(Float.toString(sp.getFloat("variableDialInner", 4.5f)));
        txtVariableDialOuter.setText(Float.toString(sp.getFloat("variableDialOuter", 0.8f)));
		chkEnableFling.setChecked(sp.getBoolean("enableFling", false));
		txtFlingDistance.setText(Integer.toString(sp.getInt("flingDistance", 200)));
		txtFlingTime.setText(Long.toString(sp.getLong("flingTime", 250l)));
		txtStartSpeed.setText(Integer.toString(sp.getInt("startSpeed", 10)));
		txtEndSpeed.setText(Integer.toString(sp.getInt("endSpeed", 0)));
		txtSpinAnimationTime.setText( Long.toString(sp.getLong("spinAnimationTime", 5000l)));

        btnCloseAndDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAction();
                onClickListener.onClick(getDialog(), 0);
            }
        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    private void closeAction() {

		//Set Default Values if Empty
		if(txtPrecision.getText().toString().isEmpty() == true) {
			txtPrecision.setText("0.5");}
		if(txtAngleSnap.getText().toString().isEmpty() == true) {
			txtAngleSnap.setText("0.125");}
		if(txtAngleSnapProximity.getText().toString().isEmpty() == true) {txtAngleSnapProximity.setText("0.03125");}
		if(txtMinimumAngle.getText().toString().isEmpty() == true) {
			txtMinimumAngle.setText("-2.45");}
		if(txtMaximumAngle.getText().toString().isEmpty() == true) {
			txtMaximumAngle.setText("3.2");}
		if(txtVariableDialInner.getText().toString().isEmpty() == true) {txtVariableDialInner.setText("4.5");}
		if(txtVariableDialOuter.getText().toString().isEmpty() == true) {txtVariableDialOuter.setText("0.8");}
		if(txtFlingDistance.getText().toString().isEmpty() == true) {txtFlingDistance.setText("200");}
		if(txtFlingTime.getText().toString().isEmpty() == true) {txtFlingTime.setText("250");}
		if(txtStartSpeed.getText().toString().isEmpty() == true) {txtStartSpeed.setText("10");}
		if(txtEndSpeed.getText().toString().isEmpty() == true) {txtEndSpeed.setText("0");}
		if(txtSpinAnimationTime.getText().toString().isEmpty() == true) {txtSpinAnimationTime.setText("5000");}

		//Copy form values to sharedPrefs
        sp.edit().putBoolean("cumulativeRotate", chkCumulativeEnable.isChecked()).commit();
        sp.edit().putBoolean("isSingleFinger", chkSingleFingerEnable.isChecked()).commit();
        sp.edit().putFloat("precisionRotation", Float.parseFloat(txtPrecision.getText().toString())).commit();
		sp.edit().putBoolean("precisionRotationEnable", chkPrecisionEnable.isChecked()).commit();
        sp.edit().putFloat("angleSnapBaseOne", Float.parseFloat(txtAngleSnap.getText().toString())).commit();
        sp.edit().putFloat("angleSnapProximity", Float.parseFloat(txtAngleSnapProximity.getText().toString())).commit();
        sp.edit().putFloat("minimumRotation", Float.parseFloat(txtMinimumAngle.getText().toString())).commit();
        sp.edit().putFloat("maximumRotation", Float.parseFloat(txtMaximumAngle.getText().toString())).commit();
        sp.edit().putBoolean("useAngleSnap", chkAngleSnapEnable.isChecked()).commit();
        sp.edit().putBoolean("useMinMaxRotate", chkMinMaxEnable.isChecked()).commit();
        sp.edit().putBoolean("useVariableDial", chkVariableDialEnable.isChecked()).commit();
		sp.edit().putBoolean("useVariableDialCurve", chkUseVariableDialCurve.isChecked()).commit();
		sp.edit().putBoolean("positiveCurve", chkPositiveCurve.isChecked()).commit();
        sp.edit().putFloat("variableDialInner", Float.parseFloat(txtVariableDialInner.getText().toString())).commit();
        sp.edit().putFloat("variableDialOuter", Float.parseFloat(txtVariableDialOuter.getText().toString())).commit();
		sp.edit().putBoolean("enableFling", chkEnableFling.isChecked()).commit();
		sp.edit().putInt("flingDistance", Integer.parseInt(txtFlingDistance.getText().toString())).commit();
		sp.edit().putLong("flingTime", Long.parseLong(txtFlingTime.getText().toString())).commit();
		sp.edit().putInt("startSpeed", Integer.parseInt(txtStartSpeed.getText().toString())).commit();
		sp.edit().putInt("endSpeed", Integer.parseInt(txtEndSpeed.getText().toString())).commit();
		sp.edit().putLong("spinAnimationTime", Long.parseLong(txtSpinAnimationTime.getText().toString())).commit();
		sp.edit().putBoolean("enableFling", chkEnableFling.isChecked()).commit();

    }


    @Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);

		closeAction();
		onClickListener.onClick(getDialog(), 0);
		onClickListener = null;

    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

}