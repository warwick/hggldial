/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;

import warwickwestonwright.hggldial.HGGLDial;
import warwickwestonwright.hggldial.HGGLDialInfo;
import warwickwestonwright.hggldial.HGGLRender;
import warwickwestonwright.hggldial.ImageUtilities;

public class DistortionFragment extends Fragment /* implements HGGLDial.IHGRotate */ {

    /*Developer Note: Alternatively as an additional usage you can use this code and call registerCallback(this);
    @Override
    public void onDown(HGGLDialInfo hgglDialInfo) {}
    @Overrideg5
    public void onPointerDown(HGGLDialInfo hgglDialInfo) {}
    @Override
    public void onMove(HGGLDialInfo hgglDialInfo) {}
    @Override
    public void onPointerUp(HGGLDialInfo hgglDialInfo) {}
    @Override
    public void onUp(HGGLDialInfo hgglDialInfo) {}
    */

    public DistortionFragment() {}

    private SharedPreferences sp;
    private HGGLDial hgglDialDistort;
    private HGGLRender HGGLRenderDistort;
    private static Bitmap bitmap;
    private AssetManager assetManager;
    private InputStream assetStream;
    private static Point point;
    private View rootView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.distortion_fragment, container, false);

        sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        assetManager = getActivity().getAssets();

        try {

            assetStream = assetManager.open("distortion_dial.png");
            bitmap = BitmapFactory.decodeStream(assetStream);
            bitmap = ImageUtilities.getProportionalBitmap(bitmap, (int) (point.x / 1.2f), "X");

        }
        catch(IOException e) {

            e.printStackTrace();

        }

        setupDistortionFragmentView();

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    private void setupDistortionFragmentView() {

        /*Developer Note: The code in this function demonstrates class usage 3 (not yet documented). This is only useful for when you need only one dial.
        To use this, you must not use a reference to the drawable in your XML Layout

        /*The following line of code is a convenience method to help handle dynamic resizing on screen rotation.
        Feel free to experiment and change the value to false and see what happens. This line of code relates to the code in the onResume.*/
        HGGLRender.setAdjustViewBounds(true);

        HGGLRenderDistort = new HGGLRender(getResources(), R.drawable.distortion_dial);
        hgglDialDistort = (HGGLDial) rootView.findViewById(R.id.hgGLDistortionView);
        hgglDialDistort.setRenderer(HGGLRenderDistort);

        /*Un-remarking the code in the onDown and onUp callback will demonstrate the suppress render capability.
        This will cause the dial to NOT move but the gesture will still track your gesture and move the the angle on release*/

        //hgglDialDistort.registerCallback(this);//See Developer note at the top of the class regarding this line and the line below.

        hgglDialDistort.registerCallback(new HGGLDial.IHGRotate() {
            @Override
            public void onDown(HGGLDialInfo hgglDialInfo) {

                //hgglDialDistort.requestRender();
                //hgglDialDistort.setSuppressRender(true);

            }
            @Override
            public void onPointerDown(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onMove(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onPointerUp(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onUp(HGGLDialInfo hgglDialInfo) {

                //hgglDialDistort.setSuppressRender(false);
                //hgglDialDistort.requestRender();

            }
        });

    }//End private void setupDistortionFragmentView()


    private void setUpPreferenceValues() {

        hgglDialDistort.setCumulativeRotate(sp.getBoolean("cumulativeRotate", true));
        hgglDialDistort.setIsSingleFinger(sp.getBoolean("isSingleFinger", true));
        hgglDialDistort.setPrecisionRotation(sp.getFloat("precisionRotation", 1.0f));

        if(sp.getBoolean("useAngleSnap", false) == true) {

            hgglDialDistort.setAngleSnapBaseOne(sp.getFloat("angleSnapBaseOne", 0.125f), sp.getFloat("angleSnapProximity", 0.03125f));

        }
        else {

            hgglDialDistort.setAngleSnapBaseOne(0, 0);

        }//End if(sp.getBoolean("useAngleSnap", false) == true)

        if(sp.getBoolean("useMinMaxRotate", false) == true) {

            hgglDialDistort.setMinMaxDial(sp.getFloat("minimumRotation", -4.0f), sp.getFloat("maximumRotation", 3.0f), sp.getBoolean("useMinMaxRotate", false));

        }
        else {

            hgglDialDistort.setMinMaxDial(0, 0, sp.getBoolean("useMinMaxRotate", false));

        }//End if(sp.getBoolean("useMinMaxRotate", false) == true)

        hgglDialDistort.setVariableDial(sp.getFloat("variableDialInner", 4.0f), sp.getFloat("variableDialOuter", 1.0f), sp.getBoolean("useVariableDial", false));

        if(sp.getBoolean("useVariableDialCurve", false) == true) {

			hgglDialDistort.setUseVariableDialCurve(true, sp.getBoolean("positiveCurve", false));

        }
        else if(sp.getBoolean("useVariableDialCurve", false) == false) {

			hgglDialDistort.setUseVariableDialCurve(false, sp.getBoolean("positiveCurve", false));

        }//End if(sp.getBoolean("useVariableDialCurve", false) == true)

        if(sp.getBoolean("enableFling", false) == true) {

			hgglDialDistort.setFlingTolerance(sp.getInt("flingDistance", 200), sp.getLong("flingTime", 250l));
			hgglDialDistort.setSpinAnimation(sp.getInt("startSpeed", 10), sp.getInt("endSpeed", 0), sp.getLong("spinAnimationTime", 5000l));

        }
        else if(sp.getBoolean("enableFling", false) == false) {

			hgglDialDistort.setFlingTolerance(0, 0l);
			hgglDialDistort.setSpinAnimation(0, 0, 0l);

        }//End if(sp.getBoolean("enableFling", false) == true)

    }//End private void setUpPreferenceValues()


    @Override
    public void onPause() {
        super.onPause();

        //Boilerplate GLView code straight out of the Android docs
        hgglDialDistort.onPause();

    }


    @Override
    public void onResume() {
        super.onResume();

        setUpPreferenceValues();

        /* The onResume gets called when the device rotates; causing the dial to distort accordingly. The point object is picked
        up in the onCreateView when the device is rotated and sets the distortions accordingly */

        if(point.x < point.y) {

            //Simple usage just give it a scaleX or ScaleY
            hgglDialDistort.setDistortDialY((float) point.y / (float) point.x);

        }
        else {

            //Simple usage just give it a scaleX or ScaleY
            hgglDialDistort.setDistortDialX((float) point.x / (float) point.y);

        }

        //Boilerplate GLView code straight out of the Android docs
        hgglDialDistort.onResume();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }


    @Override
    public void onDetach() {
        super.onDetach();

    }

}
