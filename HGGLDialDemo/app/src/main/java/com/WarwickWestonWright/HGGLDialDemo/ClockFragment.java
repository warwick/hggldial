/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import warwickwestonwright.hggldial.HGGLDial;
import warwickwestonwright.hggldial.HGGLDialInfo;
import warwickwestonwright.hggldial.HGGLRender;
import warwickwestonwright.hggldial.ImageUtilities;
import warwickwestonwright.hggldial.MultipleDials;

public class ClockFragment extends Fragment {

	public ClockFragment() {}

	private ClockFragmentHandler clockFragmentHandler = new ClockFragmentHandler(this);
	private HGGLDial hggldialtime;
	private HGGLRender hgglRender;
	private SharedPreferences sp;
	private Bitmap bitmapHour;
	private Bitmap bitmapMinute;
	private Bitmap bitmapSecond;
	private MultipleDials[] multipleDials;
	private AssetManager assetManager;
	private InputStream assetStream;
	private final Point point = new Point(0, 0);
	private RadioGroup rdoGrpTimeSelection;
	private static double currentAngle;
	private static final int SECONDS_IN_12_HOURS = 43200;
	private static final int SECONDS_IN_1_HOUR = 3600;
	private static final int CHK_HOUR = 1;
	private static final int CHK_MINUTE = 2;
	private static final int CHK_SECOND = 3;
	private static final int CHK_KEEP_TIME = 4;
	private int whatRadioButtonIsChecked;
	private volatile boolean keepTime;
	private final int[] timePartsInt = new int[3];
	private String[] timePartsStr = new String[3];
	private View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.clock_fragment, container, false);

		sp = PreferenceManager.getDefaultSharedPreferences(getContext());
		whatRadioButtonIsChecked = CHK_HOUR;
		currentAngle = 0d;
		rdoGrpTimeSelection = (RadioGroup) rootView.findViewById(R.id.rdoGrpTimeSelection);
		point.x = getResources().getDisplayMetrics().widthPixels;
		point.y = getResources().getDisplayMetrics().heightPixels;
		assetManager = getActivity().getAssets();

		try {

			multipleDials = new MultipleDials[] {
				new MultipleDials(),
				new MultipleDials(),
				new MultipleDials()
			};

			assetStream = assetManager.open("hour_hand.png");
			bitmapHour = BitmapFactory.decodeStream(assetStream);
			bitmapHour = ImageUtilities.getProportionalBitmap(bitmapHour, (int) (point.x / 1.03f), "X");
			multipleDials[0].setBitmap(bitmapHour);//Image for the hour dial
			multipleDials[0].setPrecision(1d);//Rotations in 12 hours
			multipleDials[0].setOffset(new Point(0, 0));//Rotation origin

			assetStream = assetManager.open("minute_hand.png");
			bitmapMinute = BitmapFactory.decodeStream(assetStream);
			bitmapMinute = ImageUtilities.getProportionalBitmap(bitmapMinute, (int) (point.x / 1.13f), "X");
			multipleDials[1].setBitmap(bitmapMinute);//Image for the minute dial
			multipleDials[1].setPrecision(12d);//Minutes in 12 hours
			multipleDials[1].setOffset(new Point(0, 0));//Rotation origin

			assetStream = assetManager.open("second_hand.png");
			bitmapSecond = BitmapFactory.decodeStream(assetStream);
			bitmapSecond = ImageUtilities.getProportionalBitmap(bitmapSecond, (int) (point.x / 1.23f), "X");
			multipleDials[2].setBitmap(bitmapSecond);//Image for the second dial
			multipleDials[2].setPrecision(720d);//Seconds in 12 hours

			//multipleDials[2].setOffset(new Point(200, 200));

            /* Developer Note: Replace the line below with the one above and un comment ‘//hggldialtime.setTouchOffset(200, 200);’ in the ‘setupTimeFragmentView()’
            function. This will demonstrate how you can move the rotation centre of the minute image. Leaving the setTouchOffset commented out will cause the images
            to move while the rotation origin remains in the centre. Touch offset and rotation origin are separate entities in this library. */

			multipleDials[2].setOffset(new Point(0, 0));//Rotation origin

		}
		catch (IOException e) {

			e.printStackTrace();

		}

		rdoGrpTimeSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				if(getResources().getResourceEntryName(checkedId).equals("rdoButtonHour")) {

					whatRadioButtonIsChecked = CHK_HOUR;

					if(keepTime == true) {

						keepTime = false;

					}
					else if(keepTime == false) {

						setChronologicalPrecision(whatRadioButtonIsChecked);

					}//End if(keepTime == true)

				}
				else if(getResources().getResourceEntryName(checkedId).equals("rdoButtonMinute")) {

					whatRadioButtonIsChecked = CHK_MINUTE;

					if(keepTime == true) {

						keepTime = false;

					}
					else if(keepTime == false) {

						setChronologicalPrecision(whatRadioButtonIsChecked);

					}//End if(keepTime == true)

				}
				else if(getResources().getResourceEntryName(checkedId).equals("rdoButtonSecond")) {

					whatRadioButtonIsChecked = CHK_SECOND;

					if(keepTime == true) {

						keepTime = false;

					}
					else if(keepTime == false) {

						setChronologicalPrecision(whatRadioButtonIsChecked);

					}//End if(keepTime == true)

				}
				else if(getResources().getResourceEntryName(checkedId).equals("rdoButtonKeepTime")) {

					keepTime = true;
					keepTime();

				}//End if(getResources().getResourceEntryName(checkedId).equals("rdoButtonHour"))

			}
		});

		setupTimeFragmentView();
		hggldialtime.setPrecisionRotation(1d);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setupTimeFragmentView() {

        /*Developer Note: The code in this method is the class usage for setting up a view with multiple dials. This is listed in the developer
        docs under class usage 2. When using this usage do not put a drawable attribute in the XML layout.*/
		hgglRender = new HGGLRender(getResources(), multipleDials);
		hggldialtime = (HGGLDial) rootView.findViewById(R.id.hgGLHourView);

		//See Developer Note in the onCreateView to see what the line of code below does when un commented
		//hggldialtime.setTouchOffset(200, 200);

		hggldialtime.setPrecisionRotation(1f);
		hggldialtime.setRenderer(hgglRender);
		hggldialtime.setQuickTapTime(200);

		hggldialtime.registerCallback(new HGGLDial.IHGRotate() {
			@Override
			public void onDown(HGGLDialInfo hgglDialInfo) {

				if(keepTime == true) {

					keepTime = false;
					setChronologicalPrecision(CHK_HOUR);
					hggldialtime.doManualObjectDial(getAngleFromTime(timePartsInt));
					setChronologicalPrecision(whatRadioButtonIsChecked);

				}//End if(keepTime == true)

			}
			@Override
			public void onPointerDown(HGGLDialInfo hgglDialInfo) {}
			@Override
			public void onMove(HGGLDialInfo hgglDialInfo) {}
			@Override
			public void onPointerUp(HGGLDialInfo hgglDialInfo) {}
			@Override
			public void onUp(HGGLDialInfo hgglDialInfo) {

                /* This code snippet is just for optimisation. It prevents the library from calculating bigger numbers than it needs to. */
				currentAngle = hggldialtime.getGestureFullAngle();

				if(currentAngle > 43200d) {

					currentAngle %= 1;
					hggldialtime.doManualObjectDial(currentAngle);

				}
				else if(currentAngle < -43200d) {

					currentAngle %= 1;
					hggldialtime.doManualObjectDial(currentAngle);

				}//End if(currentAngle > 43200d)

			}
		});

		if(sp.getBoolean("enableFling", false) == true) {

			hggldialtime.setFlingTolerance(sp.getInt("flingDistance", 200), sp.getLong("flingTime", 250l));
			hggldialtime.setSpinAnimation(sp.getInt("startSpeed", 10), sp.getInt("endSpeed", 0), sp.getLong("spinAnimationTime", 5000l));

		}
		else if(sp.getBoolean("enableFling", false) == false) {

			hggldialtime.setFlingTolerance(0, 0l);
			hggldialtime.setSpinAnimation(0, 0, 0l);

		}//End if(sp.getBoolean("enableFling", false) == true)

	}//End private void setupTimeFragmentView()


	private void setChronologicalPrecision(int chronologicalPrecision) {

		if(chronologicalPrecision == CHK_HOUR) {

			hggldialtime.setPrecisionRotation(1d);
			multipleDials[0].setPrecision(1d);
			multipleDials[1].setPrecision(12d);
			multipleDials[2].setPrecision(720d);

		}
		else if(chronologicalPrecision == CHK_MINUTE) {

			hggldialtime.setPrecisionRotation(1d / 12d);
			multipleDials[0].setPrecision(1d / 12d);
			multipleDials[1].setPrecision(1d);
			multipleDials[2].setPrecision(60d);

		}
		else if(chronologicalPrecision == CHK_SECOND) {

			hggldialtime.setPrecisionRotation(1d / 720d);
			multipleDials[0].setPrecision(1d / 720d);
			multipleDials[1].setPrecision(1d / 60d);
			multipleDials[2].setPrecision(1d);

		}
		else if(chronologicalPrecision == CHK_KEEP_TIME) {

			keepTime = true;

		}//End if(chronologicalPrecision == CHK_HOUR)

	}//End private void setChronologicalPrecision(int chronoPrecision)


	private void keepTime() {

		new Thread(new Runnable() {
			@Override
			public void run() {

				clockFragmentHandler.sendEmptyMessage(0);

				while(keepTime == true) {

					final long currentTimePlus100 = System.currentTimeMillis() + 100;
					while(currentTimePlus100 > System.currentTimeMillis()) {if(keepTime == false) {break;}}
					clockFragmentHandler.sendEmptyMessage(0);

				}//End while(keepTime == true)

			}
		}).start();

	}//End private void keepTime()


	private int[] getTimeParsFromTime() {

		Date date = new Date();//Replace this line for the line in the try block to experiment with hard coded times.
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		String now = dateFormat.format(date);
		String timePart = now.split(" ")[1];
		timePartsStr = timePart.split("-");
		timePartsInt[0] = Integer.parseInt(timePartsStr[0]) % 12;
		timePartsInt[1] = Integer.parseInt(timePartsStr[1]);
		timePartsInt[2] = Integer.parseInt(timePartsStr[2]);
		return timePartsInt;

	}//End private int[] getTimeParsFromTime()


	//This function has now been fully tested and is here for your convenience
	private int[] getTimeFromAngle(final double timeAsAngle) {

		final int[] returnTime = new int[3];
		int secondsInTimeAsAngle;

		if(timeAsAngle < 0) {

			secondsInTimeAsAngle = (int) (SECONDS_IN_12_HOURS * (1d + (timeAsAngle % 1d)));
			returnTime[0] = ((int) (12d * (1d + (timeAsAngle % 1d)))) % 12;

		}
		else {

			secondsInTimeAsAngle = (int) (SECONDS_IN_12_HOURS * (timeAsAngle % 1d));
			returnTime[0] = ((int) (12d * timeAsAngle)) % 12;

		}

		returnTime[2] = ((secondsInTimeAsAngle % SECONDS_IN_1_HOUR) % 60);
		returnTime[1] = ((secondsInTimeAsAngle - returnTime[2]) / 60) % 60;

		return returnTime;

	}//End private int[] getTimeFromAngle(final double timeAsAngle)


	//This function has now been fully tested and is here for your convenience
	private double getAngleFromTime(int[] timeParts) {

		return (double) ((timeParts[0] * SECONDS_IN_1_HOUR) + (timeParts[1] * 60) + timeParts[2]) / SECONDS_IN_12_HOURS;

	}


	@Override
	public void onPause() {
		super.onPause();

		//Boilerplate GLView code straight out of the Android docs
		hggldialtime.onPause();

	}


	@Override
	public void onResume() {
		super.onResume();

		//Boilerplate GLView code straight out of the Android docs
		hggldialtime.onResume();

	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

	}


	@Override
	public void onDetach() {
		super.onDetach();

		if(whatRadioButtonIsChecked == CHK_HOUR) {

			((RadioButton) rdoGrpTimeSelection.findViewById(R.id.rdoButtonHour)).setChecked(true);

		}
		else if(whatRadioButtonIsChecked == CHK_MINUTE) {

			((RadioButton) rdoGrpTimeSelection.findViewById(R.id.rdoButtonMinute)).setChecked(true);

		}
		else if(whatRadioButtonIsChecked == CHK_SECOND) {

			((RadioButton) rdoGrpTimeSelection.findViewById(R.id.rdoButtonSecond)).setChecked(true);


		}//End if(whatRadioButtonIsChecked == CHK_HOUR)

		setChronologicalPrecision(whatRadioButtonIsChecked);
		keepTime = false;
		hggldialtime.resetHGGLDial();

	}//End public void onDetach()


	private class ClockFragmentHandler extends Handler {

		private final WeakReference<ClockFragment> clockFragmentWeakReference;

		public ClockFragmentHandler(ClockFragment clockFragment) {

			clockFragmentWeakReference = new WeakReference<>(clockFragment);

		}

		@Override
		public void handleMessage(Message msg) {

			if(clockFragmentWeakReference != null) {

				final ClockFragment clockFragment = clockFragmentWeakReference.get();
				clockFragment.hggldialtime.setPrecisionRotation(1d);
				clockFragment.multipleDials[0].setPrecision(1d);
				clockFragment.multipleDials[1].setPrecision(12d);
				clockFragment.multipleDials[2].setPrecision(720d);
				clockFragment.hggldialtime.doManualObjectDial(getAngleFromTime(getTimeParsFromTime()));

				if(clockFragment.keepTime == false) {

					clockFragment.rdoGrpTimeSelection.post(new Runnable() {
						@Override
						public void run() {

							if(clockFragment.whatRadioButtonIsChecked == clockFragment.CHK_HOUR) {

								((RadioButton) clockFragment.rdoGrpTimeSelection.findViewById(R.id.rdoButtonHour)).setChecked(true);

							}
							else if(clockFragment.whatRadioButtonIsChecked == clockFragment.CHK_MINUTE) {

								((RadioButton) clockFragment.rdoGrpTimeSelection.findViewById(R.id.rdoButtonMinute)).setChecked(true);

							}
							else if(clockFragment.whatRadioButtonIsChecked == clockFragment.CHK_SECOND) {

								((RadioButton) clockFragment.rdoGrpTimeSelection.findViewById(R.id.rdoButtonSecond)).setChecked(true);

							}//End if(whatRadioButtonIsChecked == CHK_HOUR)

							setChronologicalPrecision(clockFragment.whatRadioButtonIsChecked);

						}
					});

				}//End if(clockFragment.keepTime == false)

			}//End if(clockFragmentWeakReference != null)

		}

	}//End private class ClockFragmentHandler extends Handler

}