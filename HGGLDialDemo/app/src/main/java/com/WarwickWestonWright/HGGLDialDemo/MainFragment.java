/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer.
Should you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGGLDialDemo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import warwickwestonwright.hggldial.HGGLDial;
import warwickwestonwright.hggldial.HGGLDialInfo;
import warwickwestonwright.hggldial.ImageUtilities;
import warwickwestonwright.hggldial.MultipleDials;

public class MainFragment extends Fragment implements DialogInterface.OnClickListener {

    private static final int DISTORTION_DEMO = 3;
    private static final int CLOCK_DEMO = 2;
    private static final int MAIN_DEMO = 1;
    private static final int NO_SELECTION = 0;
    private static Point point;
    private static float snapTolerance = 0.045f;
    private static int selection = 0;
    private IMainFragment iMainFragment;
    private HGGLDial hgglDial;
    private MultipleDials[] multipleDials;
    private Bitmap bitmap;
    private AssetManager assetManager;
    private InputStream assetStream;
    private TextView lblSelectedMenu;
    private View rootView;

    public MainFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.main_fragment, container, false);

        point = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        assetManager = getActivity().getAssets();
        lblSelectedMenu = (TextView) rootView.findViewById(R.id.lblSelectedMenu);

        try {

            multipleDials = new MultipleDials[] {new MultipleDials()};

            assetStream = assetManager.open("your_dial_image_here.png");
            bitmap = BitmapFactory.decodeStream(assetStream);
            bitmap = ImageUtilities.getProportionalBitmap(bitmap, point.x, "X");

            multipleDials[0].setBitmap(bitmap);
            multipleDials[0].setOffset(new Point(0, 0));
            multipleDials[0].setPrecision(1f);

        }
        catch(IOException e) {

            e.printStackTrace();

        }

        setupMainFragmentView();

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    private void setupMainFragmentView() {

        /*Developer Note: The code in this function demonstrates class usage 1 as laid out in the library docs. This is only useful for when you need only one dial.
        It's a nice simple one liner. To use this you require the following lines of code in your XML Layout
        1. app:hg_drawable="@drawable/distortion_dial" This must be placed in the custom view: warwickwestonwright.hggldial.HGGLDial
        2. xmlns:app="http://schemas.android.com/apk/res-auto". Which should be placed in the root view but can be placed in the custom controls parent FrameLayout*/

        hgglDial = (HGGLDial) rootView.findViewById(R.id.hgGLRotateView);
        hgglDial.setPrecisionRotation(1f);
        hgglDial.setAngleSnapBaseOne(0.25f, 0.045f);
        hgglDial.setMinMaxDial(1f, 3f, true);
        hgglDial.doManualGestureDial(2f);

        hgglDial.registerCallback(new HGGLDial.IHGRotate() {
            @Override
            public void onDown(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onPointerDown(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onMove(HGGLDialInfo hgglDialInfo) {

                /* This snippet is quite intuitive and doesn't need much explaining, outer condition check
                to see if  angle is in snap zone and the inner conditions check what angles they are snapped to*/
                if(hgglDialInfo.getRotateSnapped() == true) {

                    //Check to see if rotated left.
                    if(hgglDialInfo.getObjectAngleBaseOne() > 0.75f - snapTolerance && hgglDialInfo.getObjectAngleBaseOne() < 0.75f + snapTolerance) {

                        //Sets a flag that is checked in the onUp and dynamically shows the current selection.
                        selection = DISTORTION_DEMO;
                        lblSelectedMenu.setText("You have selected\nDial Distortion Demo");

                    }
                    //Check to see if rotated 180 (AKA 0.5).
                    else if(hgglDialInfo.getObjectAngleBaseOne() > 0.5f - snapTolerance && hgglDialInfo.getObjectAngleBaseOne() < 0.5f + snapTolerance) {

                        //Sets a flag that is checked in the onUp and dynamically shows the current selection.
                        selection = CLOCK_DEMO;
                        lblSelectedMenu.setText("You have selected\nClock Demo");

                    }
                    //Check to see if rotated right
                    else if(hgglDialInfo.getObjectAngleBaseOne() > 0.25f - snapTolerance && hgglDialInfo.getObjectAngleBaseOne() < 0.25f + snapTolerance) {

                        //Sets a flag that is checked in the onUp and dynamically shows the current selection.
                        selection = MAIN_DEMO;
                        lblSelectedMenu.setText("You have selected\nMain Library Demo");

                    }
                    //Check to see if no rotation.
                    else if(hgglDialInfo.getObjectAngleBaseOne() > 0.0f - snapTolerance && hgglDialInfo.getObjectAngleBaseOne() < 0.0f + snapTolerance) {

                        //Sets a flag that is checked in the onUp and dynamically shows the current selection.
                        selection = NO_SELECTION;
                        lblSelectedMenu.setText("Nothing Selected!\nRotate and release to Select!");

                    }

                }
                else {

                    lblSelectedMenu.setText("Nothing Selected!\nRotate and release to Select!");

                }//if(hgResult.getRotateSnapped() == true)

            }


            @Override
            public void onPointerUp(HGGLDialInfo hgglDialInfo) {}
            @Override
            public void onUp(HGGLDialInfo hgglDialInfo) {

                //Checks to see if dial is in snap zone
                if(hgglDialInfo.getRotateSnapped() == true) {

                    //If in snap zone select the appropriate action.
                    if(selection == DISTORTION_DEMO) {

                        iMainFragment.mainFragmentCallback(DistortionFragment.class.getName());

                    }
                    else if(selection == CLOCK_DEMO) {

                        iMainFragment.mainFragmentCallback(ClockFragment.class.getName());

                    }
                    else if(selection == MAIN_DEMO) {

                        iMainFragment.mainFragmentCallback(DemoFragment.class.getName());

                    }
                    else if(selection == NO_SELECTION) {

                        lblSelectedMenu.setText("Nothing Selected!\nRotate and release to Select!");

                    }// End if(selection == DISTORTION_DEMO)

                }//End if(HGGLDialInfo.getRotateSnapped() == true)

            }
        });

    }//End private void setupMainFragmentView()

    @Override
    public void onPause() {
        super.onPause();

        //Boilerplate GLView code straight out of the Android docs
        hgglDial.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();

        //Boilerplate GLView code straight out of the Android docs
        hgglDial.onResume();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof IMainFragment) {

            iMainFragment = (IMainFragment) context;

        }
        else {

            throw new RuntimeException(context.toString() + " must implement IMainFragment");

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        iMainFragment = null;

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        dialog.dismiss();
        setupMainFragmentView();

    }


    public interface IMainFragment {

        void mainFragmentCallback(String fragmentName);

    }

}