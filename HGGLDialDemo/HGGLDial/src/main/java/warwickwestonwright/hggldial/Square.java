/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
Class Usage:
Constructor public Square(int width, int height, Bitmap[] bitmaps)
First 2 parameters are for the view port (ie Layout Width and Height). The last parameter is an array of bitmaps used for the rendering textures
*/

package warwickwestonwright.hggldial;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square {

    private static final String vertexShaderSolidHook =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private static final String fragmentShaderSolidHook =
            "precision mediump float;" +
                    "void main() {" +
                    "  gl_FragColor = vec4(0.5,0,0,1);" +
                    "}";

    private static final String vertexShaderImageHook =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "attribute vec2 a_texCoord;" +
                    "varying vec2 v_texCoord;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "  v_texCoord = a_texCoord;" +
                    "}";

    private static final String fragmentShaderImageHook =
            "precision mediump float;" +
                    "varying vec2 v_texCoord;" +
                    "uniform sampler2D s_texture;" +
                    "void main() {" +
                    "  gl_FragColor = texture2D( s_texture, v_texCoord );" +
                    "}";

    private float[] vertices;
    private final short[] verticesDrawOrder = new short[] {0, 1, 2, 0, 2, 3};
    private float[] uvs;
    private final FloatBuffer[] verticesBuffer;
    private final ShortBuffer[] drawBuffer;
    private final FloatBuffer[] uvBuffer;
    private final int[] textureImageProgram;
    private final int[] textureNames;
    private static int bitmapsLength;

    public Square(int width, int height, Bitmap[] bitmaps) {

        bitmapsLength = bitmaps.length;
        drawBuffer = new ShortBuffer[bitmapsLength];
        uvBuffer = new FloatBuffer[bitmapsLength];
        textureImageProgram = new int[bitmapsLength];
        verticesBuffer = new FloatBuffer[bitmapsLength];

        //Generate Textures, if more needed, alter these numbers.
        textureNames = new int[bitmapsLength];
        GLES20.glGenTextures(bitmapsLength, textureNames, 0);

        for(int i = 0; i < bitmapsLength; i++) {

            Bitmap bitmap = bitmaps[i].copy(Bitmap.Config.ARGB_8888, true);

            //Bind texture to textureName
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + i);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureNames[i]);

            //Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

            //Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            if(bitmap.isRecycled() == false) {bitmap.recycle();}

            int originalWidth = bitmaps[i].getWidth();
            int originalHeight = bitmaps[i].getHeight();

            Point tl = new Point((width - originalWidth) / 2, ((height / 2) + (originalHeight / 2)));
            Point bl = new Point((width - originalWidth) / 2, ((height - originalHeight) / 2));
            Point br = new Point(((width / 2) + (originalWidth / 2)), ((height - originalHeight) / 2));
            Point tr = new Point(((width / 2) + (originalWidth / 2)), ((height / 2) + (originalHeight / 2)));

            vertices = new float[] {
                    tl.x, tl.y, 1.0f,
                    bl.x, bl.y, 1.0f,
                    br.x, br.y, 1.0f,
                    tr.x, tr.y, 1.0f,
            };

            //verticesDrawOrder = new short[] {0, 1, 2, 0, 2, 3};

            //The vertex buffer.
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            verticesBuffer[i] = byteBuffer.asFloatBuffer();
            verticesBuffer[i].put(vertices);
            verticesBuffer[i].position(0);

            //initialize byte buffer for the draw list
            ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(verticesDrawOrder.length * 2);
            drawListBuffer.order(ByteOrder.nativeOrder());
            drawBuffer[i] = drawListBuffer.asShortBuffer();
            drawBuffer[i].put(verticesDrawOrder);
            drawBuffer[i].position(0);
            //UV clamping coordinates used by textures.

            uvs = new float[] {
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f
            };

            //The texture buffer
            byteBuffer = ByteBuffer.allocateDirect(uvs.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            uvBuffer[i] = byteBuffer.asFloatBuffer();
            uvBuffer[i].put(uvs);
            uvBuffer[i].position(0);

            //Solid color and image texture handles
            int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderSolidHook);
            int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderSolidHook);
            vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderImageHook);
            fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderImageHook);
            textureImageProgram[i] = GLES20.glCreateProgram();
            GLES20.glAttachShader(textureImageProgram[i], vertexShader);
            GLES20.glAttachShader(textureImageProgram[i], fragmentShader);
            GLES20.glLinkProgram(textureImageProgram[i]);
            GLES20.glUseProgram(textureImageProgram[i]);

        }//End for(int i = 0; i < bitmapsLength; i++)

    }//End public Square()


    private int loadShader(int shaderFlag, String shaderCode) {

        int shader = GLES20.glCreateShader(shaderFlag);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;

    }//End private int loadShader(int type, String shaderCode)


    public void draw(float[] mvpMatrix, int activeTexture) {

        int vPositionHandle = GLES20.glGetAttribLocation(textureImageProgram[activeTexture], "vPosition");
        //Enable the arrays
        GLES20.glEnableVertexAttribArray(vPositionHandle);
        GLES20.glVertexAttribPointer(vPositionHandle, 3, GLES20.GL_FLOAT, false, 0, verticesBuffer[activeTexture]);
        int textureLocationHandle = GLES20.glGetAttribLocation(textureImageProgram[activeTexture], "a_texCoord");
        GLES20.glEnableVertexAttribArray(textureLocationHandle);
        GLES20.glVertexAttribPointer(textureLocationHandle, 2, GLES20.GL_FLOAT, false, 0, uvBuffer[activeTexture]);
        int matrixImageTransformationHandle = GLES20.glGetUniformLocation(textureImageProgram[activeTexture], "uMVPMatrix");
        GLES20.glUniformMatrix4fv(matrixImageTransformationHandle, 1, false, mvpMatrix, 0);
        int matrixTextureHandle = GLES20.glGetUniformLocation(textureImageProgram[activeTexture], "s_texture");
        GLES20.glUniform1i(matrixTextureHandle, activeTexture);//Save the texture state
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, verticesDrawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawBuffer[activeTexture]);
        //Disable the arrays
        GLES20.glDisableVertexAttribArray(vPositionHandle);
        GLES20.glDisableVertexAttribArray(textureLocationHandle);

    }//End public void draw(float[] mvpMatrix)

}