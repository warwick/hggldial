/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
Class Usage: Constructor 1: HGRender(Resources resources, int imageResourceId): Used internally. android.content.res.Resources object
and imageResourceId (id of the drawable picked up from the XML Layout)
Constructor 2: public HGRender(Resources resources, MultipleDials[] multipleDials)
1st Parameter: android.content.res.Resources object followed by an array instance of MultipleDials (See description in MultipleDials class)
*/

package warwickwestonwright.hggldial;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class HGGLRender implements GLSurfaceView.Renderer {

    public interface IHGRender {
        void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] morphMatrix, final float[] matrixProjectionAndView);
    }

    public static IHGRender ihgRender;
    private final float[] matrixProjection = new float[16];
    private final float[] matrixView = new float[16];
    private final float[] matrixProjectionAndView = new float[16];
    private final float[] morphMatrix = new float[16];
    private static Square square = null;
    private static Bitmap[] bitmaps = null;
    private static MultipleDials[] multipleDials;
    private static boolean useRapidDial = false;
    private Resources resources;
    private static int imageResourceId;
    private static int viewPortWidth;
    private static int viewPortHeight;
    private final static Point centerPoint = new Point(0, 0);
    private static boolean adjustViewBounds = false;

    public HGGLRender(Resources resources, int imageResourceId) {

        this.bitmaps = new Bitmap[1];
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inScreenDensity = (int) resources.getDisplayMetrics().density;
        //options.inDither = true;
        options.inJustDecodeBounds = false;
        bitmaps[0] = BitmapFactory.decodeResource(resources, imageResourceId, options);
        this.multipleDials = new MultipleDials[1];
        multipleDials[0] = new MultipleDials(bitmaps[0], new Point(), 1f, 0f);
        square = null;
        this.resources = resources;
        this.imageResourceId = imageResourceId;

    }//End public HGRender(Resources resources, int imageResourceId)


    public HGGLRender(Resources resources, MultipleDials[] multipleDials) {

        this.bitmaps = new Bitmap[multipleDials.length];
        this.multipleDials = new MultipleDials[multipleDials.length];
        this.multipleDials = multipleDials;
        square = null;
        this.resources = resources;

        for(int i = 0; i < multipleDials.length; i++) {

            this.bitmaps[i] = multipleDials[i].getBitmap();

        }

    }//End public HGRender(Resources resources, MultipleDials[] multipleDials)


    public static void registerRenderer(IHGRender ihgRender) {HGGLRender.ihgRender = ihgRender;}
    public static void setAdjustViewBounds(boolean adjustViewBounds) {HGGLRender.adjustViewBounds = adjustViewBounds;}
    public static boolean getAdjustViewBounds() {return HGGLRender.adjustViewBounds;}
    public static MultipleDials[] getMultipleDials() {return HGGLRender.multipleDials;}
    public static void setUseRapidDial(boolean useRapidDial) {HGGLRender.useRapidDial = useRapidDial;}
    public static boolean getUseRapidDial() {return HGGLRender.useRapidDial;}
    public static int[] getOriginalHeightAndWidth() {return new int[] {HGGLRender.viewPortWidth, HGGLRender.viewPortHeight};}
    public static Point getCenterPoint() {return HGGLRender.centerPoint;}


    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glDisable(GLES20.GL_CULL_FACE);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glDisable(GLES20.GL_CULL_FACE);

    }//End public void onSurfaceCreated(GL10 unused, EGLConfig config)


    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {

        GLES20.glViewport(0, 0, width, height);
		centerPoint.x = width / 2;
		centerPoint.y = height / 2;

        HGGLRender.viewPortWidth = width;
        HGGLRender.viewPortHeight = height;
        HGGLDial.setCenterPoint(centerPoint);

        int newDimensionXorY = 0;

        if(adjustViewBounds == true) {

            if(width < height) {

                newDimensionXorY = width;

            }
            else {

                newDimensionXorY = height;

            }

        }//End if(adjustViewBounds == true)

        if(adjustViewBounds == true) {

            bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], newDimensionXorY, "X");

        }
        else {

            bitmaps[0] = ImageUtilities.getProportionalBitmap(bitmaps[0], width, "X");

        }

        square = new Square(width, height, bitmaps);

        for(int i = 0; i < 16; i++) {

            matrixProjection[i] = 0.0f;
            matrixView[i] = 0.0f;
            matrixProjectionAndView[i] = 0.0f;

        }

        Matrix.orthoM(matrixProjection, 0, 0f, width, 0.0f, height, - 1f, 1f);
        Matrix.setLookAtM(matrixView, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        Matrix.multiplyMM(matrixProjectionAndView, 0, matrixProjection, 0, matrixView, 0);

    }//End public void onSurfaceChanged(GL10 unused, int width, int height)


    @Override
    public void onDrawFrame(GL10 unused) {

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        for(int i = 0; i < bitmaps.length; i++) {

            ihgRender.drawFrame(square, i, centerPoint, unused, morphMatrix, matrixProjectionAndView);

        }

        //The call that sets useRapidDial to true is in the HGGLDial.doRapidDial method
        useRapidDial = false;

    }//End public void onDrawFrame(GL10 unused)

}