/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*Class Usage: Constructor 1: public HGGLDialInfo()// Default: constructor
This class is constructed internally by the library and is used in the callback. It exposes the dial status. The status returned that you need to be concerned with are listed in the
'Accessors' section of the class and need not explanation as the naming conventions give an accurate description.*/

package warwickwestonwright.hggldial;

final public class HGGLDialInfo {

    private boolean quickTap;
    private int gestureRotationDirection;
    private int objectRotationDirection;
    private double gestureAngleBaseOne;
    private double objectAngleBaseOne;
    private float firstTouchX;
    private float firstTouchY;
    private float secondTouchX;
    private float secondTouchY;
    private int objectRotationCount;
    private int gestureRotationCount;
    private double gestureTouchAngle;
    private double twoFingerDistance;
    private boolean rotateSnapped;
    private double variablePrecision;
    private int minMaxRotationOutOfBounds;
    private boolean spinTriggered;
    private float spinCurrentSpeed;

    public HGGLDialInfo() {

        this.quickTap = false;
        this.gestureRotationDirection = 0;
        this.objectRotationDirection = 0;
        this.gestureAngleBaseOne = 0f;
        this.objectAngleBaseOne = 0f;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.objectRotationCount = 0;
        this.gestureRotationCount = 0;
        this.gestureTouchAngle = 0f;
        this.rotateSnapped = false;
        this.variablePrecision = 0f;
		this.minMaxRotationOutOfBounds = 0;
		this.spinTriggered = false;
		this.spinCurrentSpeed = 0;

    }//End public HGResult()

    /* Accessors */
    public boolean getQuickTap() {return this.quickTap;}
    public int getGestureRotationDirection() {return this.gestureRotationDirection;}
    public int getObjectRotationDirection() {return this.objectRotationDirection;}
    public double getGestureAngleBaseOne() {return this.gestureAngleBaseOne;}
    public double getObjectAngleBaseOne() {return this.objectAngleBaseOne;}
    public float getFirstTouchX() {return this.firstTouchX;}
    public float getFirstTouchY() {return this.firstTouchY;}
    public float getSecondTouchX() {return this.secondTouchX;}
    public float getSecondTouchY() {return this.secondTouchY;}
    public int getObjectRotationCount() {return this.objectRotationCount;}
    public int getGestureRotationCount() {return this.gestureRotationCount;}
    public double getGestureTouchAngle() {return this.gestureTouchAngle;}
    public double getTwoFingerDistance() {return this.twoFingerDistance;}
    public boolean getRotateSnapped() {return this.rotateSnapped;}
    public double getVariablePrecision() {return this.variablePrecision;}
	public int getMinMaxRotationOutOfBounds() {return this.minMaxRotationOutOfBounds;}
	public boolean getSpinTriggered() {return this.spinTriggered;}
	public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}

    /* Mutators */
    void setQuickTap(boolean quickTap) {this.quickTap = quickTap;}
    void setGestureRotationDirection(int rotationGestureDirection) {this.gestureRotationDirection = rotationGestureDirection;}
    void setObjectRotationDirection(int rotationObjectDirection) {this.objectRotationDirection = rotationObjectDirection;}
    void setGestureAngleBaseOne(double angleGestureBaseOne) {this.gestureAngleBaseOne = angleGestureBaseOne;}
    void setObjectAngleBaseOne(double angleBaseOne) {this.objectAngleBaseOne = angleBaseOne;}
    void setFirstTouchX(float firstTouchX) {this.firstTouchX = firstTouchX;}
    void setFirstTouchY(float firstTouchY) {this.firstTouchY = firstTouchY;}
    void setSecondTouchX(float secondTouchX) {this.secondTouchX = secondTouchX;}
    void setSecondTouchY(float secondTouchY) {this.secondTouchY = secondTouchY;}
    void setObjectRotationCount(int objectRotationCount) {this.objectRotationCount = objectRotationCount;}
    void setGestureRotationCount(int fullRotationCount) {this.gestureRotationCount = fullRotationCount;}
    void setGestureTouchAngle(double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}
    void setTwoFingerDistance(double twoFingerDistance) {this.twoFingerDistance = twoFingerDistance;}
    void setRotateSnapped(boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
    void setVariablePrecision(double variablePrecision) {this.variablePrecision = variablePrecision;}
	void setMinMaxRotationOutOfBounds(int minMaxRotationOutOfBounds) {this.minMaxRotationOutOfBounds = minMaxRotationOutOfBounds;}
	void setSpinTriggered(boolean spinTriggered) {this.spinTriggered = spinTriggered;}
	void setSpinCurrentSpeed(float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}

}//End final public class HGResult