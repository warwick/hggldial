/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package warwickwestonwright.hggldial;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import java.lang.ref.WeakReference;

import javax.microedition.khronos.opengles.GL10;

import static warwickwestonwright.hggldial.HGGLGeometry.getTwoFingerDistance;

public class HGGLDial extends GLSurfaceView implements
	HGGLRender.IHGRender,
	Runnable {

    /* Top of block field declarations */
	private final HGGLDialHandler hgglDialHandler = new HGGLDialHandler(this);
    private HGGLRender hgglRender;
    private HGGLRender.IHGRender ihgRender;
    private int imageResourceId;
    private OnTouchListener onTouchListener;
    private final static Point centerPoint = new Point(0, 0);
    private static IHGRotate ihgRotate;
    private final HGGLDialInfo hgglDialInfo = new HGGLDialInfo();
    private final GetAngleWrapper getAngleWrapper = new GetAngleWrapper();
    private float firstTouchX;
    private float firstTouchY;
    private float secondTouchX;
    private float secondTouchY;
    private double storedGestureAngleBaseOne;
    private int storedObjectCount;
    private double currentGestureAngleBaseOne;
    private double fullObjectAngleBaseOne;
    private int touchPointerCount;
    private boolean cumulativeRotate;
    private boolean isSingleFinger;
    private double onDownAngleObjectCumulativeBaseOne;
    private double onUpAngleGestureBaseOne;
    private double precisionRotation;
	private double storedPrecisionRotation;
    private double angleSnapBaseOne;
    private double angleSnapNextBaseOne;
    private double angleSnapProximity;
    private boolean rotateSnapped;
    private double maximumRotation;
    private double minimumRotation;
    private boolean useMinMaxRotation;
    private int minMaxRotationOutOfBounds;
    private float touchOffsetX;
    private float touchOffsetY;
    private int[][] renderOffset = null;
    private final Point touchPoint = new Point();
    private long quickTapTime;
    private float distortDialX;
    private float distortDialY;
    private boolean suppressRender;
    private double variableDialInner;
    private double variableDialOuter;
    private boolean useVariableDial;
	private boolean useVariableDialCurve;
	private boolean positiveCurve;
	private static double imageRadius;
	private double imageRadiusX2;
    private double originalWidth;
    private double originalHeight;
    private MultipleDials[] multipleDials;

	//Spin Variables
    private float spinStartSpeed;
    private float spinEndSpeed;
    private long spinDuration;
    private long spinEndTime;
    private double spinStartAngle;
    private long gestureDownTime;//Also used for quick tap
    private float flingDownTouchX;
    private float flingDownTouchY;
    private int flingDistanceThreshold;
    private long flingTimeThreshold;
    private volatile boolean spinTriggered;
	private volatile boolean spinTriggeredProgrammatically;
    private int lastObjectDirection;
    private Thread spinAnimationThread;

    /* Bottom of block field declarations */

    /* Top of block field declarations */
    public interface IHGRotate {

        void onDown(HGGLDialInfo hgglDialInfo);
        void onPointerDown(HGGLDialInfo hgglDialInfo);
        void onMove(HGGLDialInfo hgglDialInfo);
        void onPointerUp(HGGLDialInfo hgglDialInfo);
        void onUp(HGGLDialInfo hgglDialInfo);

    }//End public interface IHGRotate

	/* Top of block main angle wrapper used by outer class */
    final public static class GetAngleWrapper {

        private static int rotationGestureDirection;
        private static int rotationObjectDirection;
        private static int rotationGestureCount;
        private static int rotationObjectCount;
        private static double angleGestureBaseOne;
        private static double angleObjectBaseOne;
        private static double gestureTouchAngle;

        public GetAngleWrapper() {

            this.rotationGestureDirection = 0;
            this.rotationObjectDirection = 0;
            this.angleGestureBaseOne = 0;
            this.angleObjectBaseOne = 0;
            this.rotationGestureCount = 0;
            this.rotationObjectCount = 0;
            this.gestureTouchAngle = 0;

        }//End public GetAngleWrapper()

        /* Accessors */
        public int getGestureRotationDirection() {return this.rotationGestureDirection;}
        public int getObjectRotationDirection() {return this.rotationObjectDirection;}
        public int getGestureRotationCount() {return this.rotationGestureCount;}
        public int getObjectRotationCount() {return this.rotationObjectCount;}
        public double getGestureAngleBaseOne() {return this.angleGestureBaseOne;}
        public double getObjectAngleBaseOne() {return (float) this.angleObjectBaseOne % 1;}
        public double getGestureTouchAngle() {return this.gestureTouchAngle;}

        /* Mutators */
        private void setGestureRotationDirection(final int rotationGestureDirection) {this.rotationGestureDirection = rotationGestureDirection;}
        private void setObjectRotationDirection(final int rotationObjectDirection) {this.rotationObjectDirection = rotationObjectDirection;}
        private void setGestureRotationCount(final int gestureRotationCount) {this.rotationGestureCount = gestureRotationCount;}
        private void setObjectRotationCount(final int objectRotationCount) {this.rotationObjectCount = objectRotationCount;}
        private void setGestureAngleBaseOne(final double angleBaseOne) {this.angleGestureBaseOne = angleBaseOne;}
        private void setObjectAngleBaseOne(final double precisionAngleBaseOne) {this.angleObjectBaseOne = precisionAngleBaseOne;}
        private void setGestureTouchAngle(final float gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

    }//End final public static class GetAngleWrapper
    /* Bottom of block main angle wrapper used by outer class */


	/* Constructor */
    public HGGLDial(Context context, AttributeSet attrs) {
        super(context, attrs);

        setFields();
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestos, 0, 0);
        setEGLContextClientVersion(2);
        setZOrderOnTop(true);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        getHolder().setFormat(PixelFormat.RGBA_8888);
        hgglRender.registerRenderer(ihgRender);

        if(a.hasValue(R.styleable.HacerGestos_hg_drawable)) {

            imageResourceId = a.getResourceId(a.getIndex(R.styleable.HacerGestos_hg_drawable), 0);
            a.recycle();
            hgglRender = new HGGLRender(getResources(), imageResourceId);
            setRenderer(hgglRender);
            setDimensionsAndMultipleDials();
            setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        }//End if(a.hasValue(R.styleable.HacerGestos_hg_drawable))

    }//End public HGGLDial(Context context, AttributeSet attrs)


    private void setDimensionsAndMultipleDials() {

        this.centerPoint.x = HGGLRender.getCenterPoint().x;
        this.centerPoint.y = HGGLRender.getCenterPoint().y;

        this.multipleDials = hgglRender.getMultipleDials();
        renderOffset = new int[multipleDials.length][];

		if(multipleDials[0] != null) {

            for(int i = 0; i < multipleDials.length; i++) {

                renderOffset[i] = new int[2];
                renderOffset[i][0] = multipleDials[i].getOffset().x;
                renderOffset[i][1] = - multipleDials[i].getOffset().y;

            }

        }

        post(new Runnable() {
            @Override
            public void run() {

                originalWidth = hgglRender.getOriginalHeightAndWidth()[0];
                originalHeight = hgglRender.getOriginalHeightAndWidth()[1];

				if(originalWidth < originalHeight) {

					imageRadius = originalHeight / 2;

				}
				else if(originalWidth >= originalHeight) {

					imageRadius = originalWidth / 2;

				}

                ((RelativeLayout) getParent()).getLayoutParams().width = (int) originalWidth;
                ((RelativeLayout) getParent()).getLayoutParams().height = (int) originalHeight;
                invalidate();

            }
        });

    }//End private void setDimensionsAndMultipleDials()


    @Override
    public void onResume() {
        super.onResume();

        setDimensionsAndMultipleDials();
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

    }


    private void setFields() {

        this.ihgRender = this;
		HGGLDial.ihgRotate = null;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.secondTouchX = 0f;
        this.secondTouchY = 0f;
        this.storedGestureAngleBaseOne = 0d;
        this.storedObjectCount = 0;
        this.currentGestureAngleBaseOne = 0d;
        this.fullObjectAngleBaseOne = 0d;
		this.touchPointerCount = 0;
        this.storedObjectCount = 0;
		this.cumulativeRotate = true;
        this.isSingleFinger = true;
        this.onDownAngleObjectCumulativeBaseOne = 0d;
        this.onUpAngleGestureBaseOne = 0d;
        this.precisionRotation = 1d;
		this.storedPrecisionRotation = 1d;
        this.angleSnapBaseOne = 0d;
        this.angleSnapNextBaseOne = 0d;
        this.angleSnapProximity = 0d;
        this.rotateSnapped = false;
        this.maximumRotation = 0d;
        this.minimumRotation = 0d;
        this.useMinMaxRotation = false;
        this.minMaxRotationOutOfBounds = 0;
        this.touchOffsetX = 0f;
        this.touchOffsetY = 0f;
        this.renderOffset = null;
        this.touchPoint.x = 0;
        this.touchPoint.y = 0;
        this.quickTapTime = 75l;
        this.gestureDownTime = 0l;
        this.distortDialX = 1f;
        this.distortDialY = 1f;
        this.suppressRender = false;
        this.variableDialInner = 4.5d;
        this.variableDialOuter = 0.8d;
        this.useVariableDial = false;
		this.useVariableDialCurve = false;
		this.positiveCurve = false;
		HGGLDial.imageRadius = 0d;
		this.imageRadiusX2 = HGGLDial.imageRadius * 2;
        this.originalWidth = 0d;
        this.originalHeight = 0d;
        this.multipleDials = null;

		//Spin Variables
		this.spinStartSpeed = 0f;
		this.spinEndSpeed = 0f;
		this.spinDuration = 0l;
		this.spinEndTime = 0l;
		this.spinStartAngle = 0d;
		this.gestureDownTime = 0l;
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0l;
		this.spinTriggered = false;
		this.spinTriggeredProgrammatically = false;
		this.lastObjectDirection = 0;
		this.spinAnimationThread = null;

    }//End private void setFields()


    public void registerCallback(IHGRotate ihgRotate) {HGGLDial.ihgRotate = ihgRotate;}


    /* Top of block touch methods */
    private void setDownTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    if(isSingleFinger == true) {

                        firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                        firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                        hgglDialInfo.setFirstTouchX(firstTouchX);
                        hgglDialInfo.setFirstTouchY(firstTouchY);
                        secondTouchX = touchOffsetX;
                        secondTouchY = touchOffsetY;

                    }//End if(isSingleFinger == true)

                }
                else if(touchPointerCount == 2) {

                    if(isSingleFinger == false) {

                        firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                        firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                        secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                        secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                        hgglDialInfo.setFirstTouchX(firstTouchX);
                        hgglDialInfo.setFirstTouchY(firstTouchY);
                        hgglDialInfo.setSecondTouchX(secondTouchX);
                        hgglDialInfo.setSecondTouchY(secondTouchY);

                    }//End if(isSingleFinger == false)

                }//End if(touchPointerCount == 1)

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setDownTouch(final MotionEvent event)


    private void setMoveTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount < 2) {

                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    hgglDialInfo.setFirstTouchX(firstTouchX);
                    hgglDialInfo.setFirstTouchY(firstTouchY);
                    secondTouchX = touchOffsetX;
                    secondTouchY = touchOffsetY;

                }
                else /* if(touchPointerCount >= 2) */ {

                    firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                    firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                    secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                    secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                    hgglDialInfo.setFirstTouchX(firstTouchX);
                    hgglDialInfo.setFirstTouchY(firstTouchY);
                    hgglDialInfo.setSecondTouchX(secondTouchX);
                    hgglDialInfo.setSecondTouchY(secondTouchY);

                }//End if(event.getPointerCount() < 2)

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setMoveTouch(final MotionEvent event)


    private void setUpTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    if(isSingleFinger == true) {

                        firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                        firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                        secondTouchX = touchOffsetX;
                        secondTouchY = touchOffsetX;
                        hgglDialInfo.setFirstTouchX(firstTouchX);
                        hgglDialInfo.setFirstTouchY(firstTouchY);

                    }//End if(isSingleFinger == true)

                }
                else if(touchPointerCount == 2) {

                    if(isSingleFinger == false) {

                        firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
                        firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
                        secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
                        secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
                        hgglDialInfo.setFirstTouchX(firstTouchX);
                        hgglDialInfo.setFirstTouchY(firstTouchY);
                        hgglDialInfo.setSecondTouchX(secondTouchX);
                        hgglDialInfo.setSecondTouchY(secondTouchY);

                    }//End if(isSingleFinger == false)

                }//End if(touchPointerCount == 1)

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


    /* Top of block rotate functions */
    private HGGLDialInfo doDownDial() {

        spinTriggered = false;
		spinTriggeredProgrammatically = false;
        hgglDialInfo.setSpinTriggered(false);

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1 && isSingleFinger == true) {

            touchPoint.x = (int) (flingDownTouchX = firstTouchX);
            touchPoint.y = (int) (flingDownTouchY = firstTouchY);

        }
        else if(touchPointerCount == 2 && isSingleFinger == false) {

            touchPoint.x = (int) secondTouchX;
            touchPoint.y = (int) secondTouchY;

        }
        else /* if(touchPointerCount > 2) */ {

            return hgglDialInfo;

        }//End if(touchPointerCount == 1 && isSingleFinger == true)

        getAngleWrapper.setGestureTouchAngle(HGGLGeometry.getAngleFromPoint(centerPoint, touchPoint));

        if(precisionRotation != 0) {

            if(cumulativeRotate == true) {

                onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

				if(useVariableDial == true) {

					fullObjectAngleBaseOne = fullObjectAngleBaseOne * precisionRotation;

				}

            }
            else if(cumulativeRotate == false) {

				minMaxRotationOutOfBounds = 0;
                onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

				if(useVariableDial == true) {

					fullObjectAngleBaseOne = getAngleWrapper.getGestureTouchAngle();

				}
				else if(useVariableDial == false) {

					fullObjectAngleBaseOne = getAngleWrapper.getGestureTouchAngle() / precisionRotation;

				}//End if(useVariableDial == true)

            }//End if(cumulativeRotate == true)

			if(useVariableDial == false) {hgglDialInfo.setVariablePrecision(storedPrecisionRotation);}

            setReturnType();
			spinStartAngle = fullObjectAngleBaseOne;

        }//End if(precisionRotation != 0)

        return hgglDialInfo;

    }//End private HGGLDialInfo doDownDial()


    private HGGLDialInfo doMoveDial() {

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1 && isSingleFinger == true) {

            touchPoint.x = (int) firstTouchX;
            touchPoint.y = (int) firstTouchY;

        }
        else if(touchPointerCount == 2 && isSingleFinger == false) {

            touchPoint.x = (int) secondTouchX;
            touchPoint.y = (int) secondTouchY;

        }
        else /* if(touchPointerCount > 2) */ {

            return hgglDialInfo;

        }//End if(touchPointerCount == 1 && isSingleFinger == true)

        getAngleWrapper.setGestureTouchAngle(HGGLGeometry.getAngleFromPoint(centerPoint, touchPoint));
        setReturnType();

        return hgglDialInfo;

    }//End private HGGLDialInfo doMoveDial()


    private HGGLDialInfo doUpDial() {

        //Prepare touches for single or dual touch
        if(touchPointerCount == 1 && isSingleFinger == true) {

            touchPoint.x = (int) firstTouchX;
            touchPoint.y = (int) firstTouchY;

        }
        else if(touchPointerCount == 2 && isSingleFinger == false) {

            touchPoint.x = (int) secondTouchX;
            touchPoint.y = (int) secondTouchY;

        }
        else /* if(touchPointerCount > 2) */ {

            return hgglDialInfo;

        }//End if(touchPointerCount == 1 && isSingleFinger == true)

		getAngleWrapper.setGestureTouchAngle(HGGLGeometry.getAngleFromPoint(centerPoint, touchPoint));

        if(precisionRotation != 0) {

            onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle())) % 1;
			if(useVariableDial == true) {precisionRotation = 1f;}
			setReturnType();

        }//End if(precisionRotation != 0)

		setSpinStatus();

		if(spinTriggered == true) {

			spinAnimationThread = new Thread(this);
			spinAnimationThread.start();

		}//End if(flingTriggered == true)

		hgglDialInfo.setGestureTouchAngle(getAngleWrapper.getGestureTouchAngle());
		hgglDialInfo.setGestureRotationDirection(0);
		hgglDialInfo.setObjectRotationDirection(0);
		if(useVariableDial == false) {hgglDialInfo.setVariablePrecision(storedPrecisionRotation);}

        return hgglDialInfo;

    }//End private HGGLDialInfo doUpDial()
	/* Bottom of block rotate functions */


    private void setReturnType() {

        if(useVariableDial == false) {

            if(useMinMaxRotation == false) {

                hgglDialInfo.setGestureRotationDirection(setRotationAngleGestureAndReturnDirection());

            }
            else if(useMinMaxRotation == true) {

                hgglDialInfo.setGestureRotationDirection(setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour());

            }//End if(useMinMaxRotation == false)

            hgglDialInfo.setGestureRotationCount(getAngleWrapper.getGestureRotationCount());
            hgglDialInfo.setGestureAngleBaseOne(getAngleWrapper.getGestureAngleBaseOne());
            hgglDialInfo.setObjectRotationDirection(lastObjectDirection);
            hgglDialInfo.setObjectRotationCount(getAngleWrapper.getObjectRotationCount());
            hgglDialInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
			hgglDialInfo.setRotateSnapped(rotateSnapped);
			hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			hgglDialInfo.setVariablePrecision(storedPrecisionRotation);
            calculateObjectAngleBaseOne();

            if(angleSnapBaseOne != 0) {

                checkNextAngleSnapBaseOne();

            }

            if(suppressRender == false) {

                requestRender();

            }

        }
        else if(useVariableDial == true) {

			if(useMinMaxRotation == false) {

				getAngleWrapper.setObjectRotationDirection(setVariableDialAngleAndReturnDirection());

			}
			else if(useMinMaxRotation == true) {

				getAngleWrapper.setObjectRotationDirection(setVariableDialAngleAndReturnDirectionForMinMaxBehaviour());

			}//End if(useMinMaxRotation == false)

            hgglDialInfo.setGestureAngleBaseOne(getAngleWrapper.getGestureTouchAngle());
            hgglDialInfo.setObjectRotationCount(getAngleWrapper.getObjectRotationCount());
			hgglDialInfo.setObjectRotationDirection(lastObjectDirection);
            hgglDialInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
			hgglDialInfo.setRotateSnapped(rotateSnapped);
			hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);

            if(angleSnapBaseOne != 0) {

                checkNextAngleSnapBaseOne();

            }

            if(suppressRender == false) {

                requestRender();

            }

        }//End if(useVariableDial == false)

        hgglDialInfo.setGestureTouchAngle(getAngleWrapper.getGestureTouchAngle());

    }//End private void setReturnType()
    /* Bottom of block rotate functions */


	private void setSpinStatus() {

		final long currentTime = System.currentTimeMillis();

		if(flingTimeThreshold != 0 && isSingleFinger == true) {

			//flingDistanceThreshold IS met
			if(getTwoFingerDistance(flingDownTouchX, flingDownTouchY, firstTouchX, firstTouchY) >= flingDistanceThreshold) {

				//If flingTimeThreshold met
				if(currentTime < gestureDownTime + flingTimeThreshold) {

					spinTriggered = true;
					hgglDialInfo.setSpinTriggered(true);
					spinEndTime = currentTime + spinDuration;

				}//End if(currentTime < gestureDownTime + flingTimeThreshold)

			}
			else {

				spinTriggered = false;
				hgglDialInfo.setSpinTriggered(false);

			}//End if(getTwoFingerDistance(flingDownTouchX, flingDownTouchY, firstTouchX, firstTouchY) < flingDistanceThreshold)

		}//End if(flingTimeThreshold != 0 && isSingleFinger == true)

	}//End private void setSpinStatus()


    /* Top of block main functions */
    private int setRotationAngleGestureAndReturnDirection() {

        final int[] returnValue = new int[1];
        currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
        final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.75f)) {

            if(angleDifference > 0) {

                returnValue[0] = -1;
                fullObjectAngleBaseOne -= (angleDifference + 1f) % 1f;

            }
            else if(angleDifference < 0) {

                returnValue[0] = 1;
                fullObjectAngleBaseOne += -angleDifference % 1f;

            }//End if(angleDifference > 0)

			if(precisionRotation < 0) {

				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

			}
			else if(precisionRotation > 0) {

				getAngleWrapper.setObjectRotationDirection(returnValue[0]);

			}
			else {

				getAngleWrapper.setObjectRotationDirection(0);

			}//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(precisionRotation > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else {

					getAngleWrapper.setObjectRotationDirection(0);

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

        }//End if(!(Math.abs(angleDifference) > 0.75f))

        getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
        getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
        storedObjectCount = (int) (fullObjectAngleBaseOne * precisionRotation);
        getAngleWrapper.setObjectRotationCount(storedObjectCount);
        storedGestureAngleBaseOne = currentGestureAngleBaseOne;

        return returnValue[0];

    }//End private int setRotationAngleGestureAndReturnDirection(final Point touchPoint)


    private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullObjectAngleBaseOne -= (angleDifference + 1f) % 1f;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullObjectAngleBaseOne += -angleDifference % 1f;

			}//End if(angleDifference > 0)

			if(precisionRotation < 0) {

				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

			}
			else if(precisionRotation > 0) {

				getAngleWrapper.setObjectRotationDirection(returnValue[0]);

			}
			else {

				getAngleWrapper.setObjectRotationDirection(0);

			}//End if(precisionRotation < 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(precisionRotation > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else {

					getAngleWrapper.setObjectRotationDirection(0);

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		if(precisionRotation > 0) {

			if(fullObjectAngleBaseOne < minimumRotation / precisionRotation) {

				if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = minimumRotation / precisionRotation;

			}
			else if(fullObjectAngleBaseOne > maximumRotation / precisionRotation) {

				if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = maximumRotation / precisionRotation;

			}//End if(fullObjectAngleBaseOne < minimumRotation / precisionRotation)

		}
		else if(precisionRotation < 0) {

			if(-fullObjectAngleBaseOne < -(minimumRotation / precisionRotation)) {

				if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = -1;
				hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = (minimumRotation / precisionRotation);

			}
			else if(-fullObjectAngleBaseOne > -(maximumRotation / precisionRotation)) {

				if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
				minMaxRotationOutOfBounds = 1;
				hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
				fullObjectAngleBaseOne = (maximumRotation / precisionRotation);

			}//End if(-fullObjectAngleBaseOne < -(minimumRotation / precisionRotation))

		}//End if(precisionRotation > 0)

		getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
		getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
		storedObjectCount = (int) (fullObjectAngleBaseOne * precisionRotation);
		getAngleWrapper.setObjectRotationCount(storedObjectCount);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

        return returnValue[0];

    }//End private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour()


	private int setVariableDialAngleAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne += -(angleDifference * variablePrecision);

				}

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(variablePrecision < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(-lastObjectDirection);

				}//End if(variablePrecision > 0)

				hgglDialInfo.setObjectRotationDirection(lastObjectDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne % 1f);
		getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
		storedObjectCount = (int) (fullObjectAngleBaseOne);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setVariableDialAngleAndReturnDirection()


	private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

				if(minMaxRotationOutOfBounds == 0) {

					fullObjectAngleBaseOne += -(angleDifference * variablePrecision);

				}

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastObjectDirection = returnValue[0];
					getAngleWrapper.setObjectRotationDirection(lastObjectDirection);

				}
				else if(variablePrecision < 0) {

					lastObjectDirection = -returnValue[0];
					getAngleWrapper.setObjectRotationDirection(-lastObjectDirection);

				}//End if(variablePrecision > 0)

				hgglDialInfo.setObjectRotationDirection(lastObjectDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		if(fullObjectAngleBaseOne < minimumRotation) {

			if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = -1;
			hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullObjectAngleBaseOne = minimumRotation;

		}
		else if(fullObjectAngleBaseOne > maximumRotation) {

			if(spinTriggered == true) {hgglDialInfo.setSpinTriggered(false);spinTriggered = false;}//Cancel Spin Thread
			minMaxRotationOutOfBounds = 1;
			hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);
			fullObjectAngleBaseOne = maximumRotation;

		}//End if(fullGestureAngleBaseOne < minimumRotation)

		if(minMaxRotationOutOfBounds != 0) {

			if(variablePrecision > 0) {

				if(fullObjectAngleBaseOne == minimumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}
				else if(fullObjectAngleBaseOne == maximumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}//End if(fullGestureAngleBaseOne == minimumRotation)

			}
			else if(variablePrecision < 0) {

				if(fullObjectAngleBaseOne == minimumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}
				else if(fullObjectAngleBaseOne == maximumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

				}//End if(fullGestureAngleBaseOne == minimumRotation)

			}//End if(variablePrecision > 0)

		}//End if(minMaxRotationOutOfBounds != 0)

		getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne % 1f);
		getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
		storedObjectCount = (int) (fullObjectAngleBaseOne);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setVariableDialAngleAndReturnDirectionForMinMaxBehaviour()


    private double getVariablePrecision() {

        final double distanceFromCenter = getTwoFingerDistance(centerPoint.x, centerPoint.y, touchPoint.x, touchPoint.y);
		hgglDialInfo.setTwoFingerDistance(distanceFromCenter);
        double[] variablePrecision = new double[1];

        if(distanceFromCenter <= imageRadius) {

            variablePrecision[0] = ((imageRadius - Math.abs(distanceFromCenter)) / centerPoint.x) * ((variableDialInner - variableDialOuter)) + variableDialOuter;
            hgglDialInfo.setVariablePrecision(variablePrecision[0]);

        }

        return variablePrecision[0];

    }//End private double getVariablePrecision()


    private double getVariablePrecisionCurve() {

        final double distanceFromCenter = HGGLGeometry.getTwoFingerDistance(centerPoint.x, centerPoint.y, touchPoint.x, touchPoint.y);
		hgglDialInfo.setTwoFingerDistance(distanceFromCenter);
        double[] variablePrecision = new double[1];

        if(distanceFromCenter <= imageRadius) {

            final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);

            if(positiveCurve == false) {

                variablePrecision[0] = -rangeDiff * (1f - (Math.sin((distanceFromCenter / imageRadiusX2) * Math.PI))) + variableDialOuter;
                hgglDialInfo.setVariablePrecision(variablePrecision[0]);

            }
            else if(positiveCurve == true) {

                variablePrecision[0] = -rangeDiff * ((float) Math.cos((distanceFromCenter / imageRadiusX2) * Math.PI)) + variableDialOuter;
                hgglDialInfo.setVariablePrecision(variablePrecision[0]);

            }//End if(positiveCurve == false)

        }//End if(distanceFromCenter <= imageRadius)

        return variablePrecision[0];

    }//End private double getVariablePrecisionCurve()
    /* Bottom of block main functions */


    private void calculateObjectAngleBaseOne() {getAngleWrapper.setObjectAngleBaseOne((getGestureFullAngle() * precisionRotation) % 1);}
    /* Bottom of block Angle methods */


    /* Top of block Accessors */
    public GetAngleWrapper getAngleWrapper() {return this.getAngleWrapper;}
    public long getQuickTapTime() {return this.quickTapTime;}
    public boolean getCumulativeRotate() {return this.cumulativeRotate;}
    public boolean getIsSingleFinger() {return this.isSingleFinger;}
    public double getPrecisionRotation() {return this.precisionRotation;}
    public double getAngleSnapBaseOne() {return this.angleSnapBaseOne;}
    public double getAngleSnapProximity() {return this.angleSnapProximity;}
    public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
    public float getTouchOffsetX() {return this.touchOffsetX;}
    public float getTouchOffsetY() {return this.touchOffsetY;}
    public Point getCenterPoint() {return this.centerPoint;}
    public float getDistortDialX() {return this.distortDialX;}
    public float getDistortDialY() {return this.distortDialY;}
    public boolean hasAngleSnapped() {return rotateSnapped;}
    public boolean getSuppressRender() {return this.suppressRender;}
    public double getVariableDialInner() {return this.variableDialInner;}
    public double getVariableDialOuter() {return this.variableDialOuter;}
    public boolean getUseVariableDial() {return this.useVariableDial;}
    public double getGestureFullAngle() {return this.fullObjectAngleBaseOne;}
    public boolean getUseVariableDialCurve() {return this.useVariableDialCurve;}
    public boolean getPositiveCurve() {return this.positiveCurve;}
	public int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
	public long getFlingTimeThreshold() {return this.flingTimeThreshold;}
	public float getSpinStartSpeed() {return this.spinStartSpeed;}
	public float getSpinEndSpeed() {return this.spinEndSpeed;}
	public long getSpinDuration() {return this.spinDuration;}
	public boolean getSpinTriggered() {return this.spinTriggered;}


	//Deprecate this method as it is available in the callback return type
    private double getVariablePrecision(final Point touchPointLocal) {

        final double distanceFromCenter = getTwoFingerDistance(centerPoint.x, centerPoint.y, touchPointLocal.x, touchPointLocal.y);
        final double returnValue = (((this.imageRadius - Math.abs(distanceFromCenter)) / (float) centerPoint.x) * ((variableDialInner - variableDialOuter)) + variableDialOuter);
        return returnValue;

    }//End private float getVariablePrecision(final Point touchPointLocal)
    /* Bottom of block Accessors */


    /* Top of block Mutators */
    public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
    public void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
    public void setIsSingleFinger(final boolean isSingleFinger) {this.isSingleFinger = isSingleFinger;}
	public void setMultipleDials(final MultipleDials[] multipleDials) {this.multipleDials = multipleDials;}
	public void setDistortDialX(final float distortDialX) {this.distortDialX = distortDialX;}
	public void setDistortDialY(final float distortDialY) {this.distortDialY = distortDialY;}
	public void setSuppressRender(final boolean suppressRender) {this.suppressRender = suppressRender;}
	public void setUseVariableDialCurve(final boolean useVariableDialCurve, boolean positiveCurve) {this.useVariableDialCurve = useVariableDialCurve; this.positiveCurve = positiveCurve;}
	public void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {this.flingDistanceThreshold = flingDistanceThreshold; this.flingTimeThreshold = flingTimeThreshold;}
	public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {this.spinStartSpeed = spinStartSpeed; this.spinEndSpeed = spinEndSpeed; this.spinDuration = spinDuration;}


    public void setPrecisionRotation(final double precisionRotation) {

		this.storedPrecisionRotation = precisionRotation;

        if(this.precisionRotation != 0 && precisionRotation != 0) {

            final double currentObjectAngle = fullObjectAngleBaseOne * this.precisionRotation;
            final double newObjectAngle = fullObjectAngleBaseOne * precisionRotation;
            fullObjectAngleBaseOne += ((currentObjectAngle - newObjectAngle) / precisionRotation);

        }

        this.precisionRotation = precisionRotation;

    }//End public void setPrecisionRotation(final double precisionRotation)


    public void setAngleSnapBaseOne(final double angleSnapBaseOne, final double angleSnapProximity) {

        this.angleSnapBaseOne = (angleSnapBaseOne) % 1;
        this.angleSnapProximity = (angleSnapProximity) % 1;

        if(angleSnapProximity > angleSnapBaseOne / 2f) {this.angleSnapProximity = angleSnapBaseOne / 2;}

    }//End public void setAngleSnapBaseOne(final double angleSnapBaseOne, final double angleSnapProximity)


    public void doRapidDial(final MultipleDials[] multipleDials) {

        hgglRender.setUseRapidDial(true);
        this.multipleDials = multipleDials;
        requestRender();

    }//End public void doRapidDial(final MultipleDials[] multipleDials)


    public void setTouchOffset(final int touchOffsetX, final int touchOffsetY) {

        this.touchOffsetX = - touchOffsetX;
        this.touchOffsetY = - touchOffsetY;

    }//End public void setTouchOffset(final int touchOffsetX, final int touchOffsetY)


    public static void setCenterPoint(final Point centerPoint) {HGGLDial.centerPoint.x = centerPoint.x; HGGLDial.centerPoint.y = centerPoint.y;}


    public void setVariableDial(final double variableDialInner, final double variableDialOuter, final boolean useVariableDial) {

		this.variableDialInner = variableDialInner;
		this.variableDialOuter = variableDialOuter;
		this.useVariableDial = useVariableDial;

        post(new Runnable() {
            @Override
            public void run() {

                if(centerPoint.x == centerPoint.y) {

                    HGGLDial.imageRadius = (float) centerPoint.x;

                }
                else if(centerPoint.x > centerPoint.y) {

                    HGGLDial.imageRadius = (float) centerPoint.y;

                }
                else if(centerPoint.x < centerPoint.y) {

                    HGGLDial.imageRadius = (float) centerPoint.x;

                }//End if(centerPoint.x == centerPoint.y)

				imageRadiusX2 = (int) (imageRadius * 2f);

            }
        });

    }//End public void setVariableDial(final double variableDialInner, final double variableDialOuter, final boolean useVariableDial)


    public void setMinMaxDial(final double minimumRotation, final double maximumRotation, boolean useMinMaxRotation) {

        this.minimumRotation = minimumRotation;
        this.maximumRotation = maximumRotation;
        this.useMinMaxRotation = useMinMaxRotation;

        if(useMinMaxRotation == true) {

            if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation) {

                this.useMinMaxRotation = false;

            }
            else {

                this.useMinMaxRotation = true;

            }//End if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation)

        }//End if(useMinMaxRotation == true)

        if(this.useMinMaxRotation == false) {minMaxRotationOutOfBounds = 0; hgglDialInfo.setMinMaxRotationOutOfBounds(minMaxRotationOutOfBounds);}

    }//End public void setMinMaxDial(final double minimumRotation, final double maximumRotation, boolean useMinMaxRotation)
    /* Bottom of block Mutators */


    private void checkNextAngleSnapBaseOne() {

        final double tempAngleSnap = Math.round(getAngleWrapper.getObjectAngleBaseOne() / angleSnapBaseOne);
        angleSnapNextBaseOne = tempAngleSnap * angleSnapBaseOne;

        if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity) {

            rotateSnapped = true;
            hgglDialInfo.setRotateSnapped(true);

        }
        else {

            angleSnapNextBaseOne = getAngleWrapper.getObjectAngleBaseOne();
            rotateSnapped = false;
            hgglDialInfo.setRotateSnapped(false);

        }//End if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity)

    }//End private void checkNextAngleSnapBaseOne()


    /* Top of block convenience methods */
    public Point getPointFromAngle(final double angle) {return HGGLGeometry.getPointFromAngle(angle, centerPoint.x);}
    private void doManualObjectDialInternal(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}
	public void resetHGGLDial() {setFields();}


    public void doManualGestureDial(final double manualDial) {

        fullObjectAngleBaseOne = manualDial;
        getAngleWrapper.setGestureRotationCount((int) manualDial);
        getAngleWrapper.setGestureAngleBaseOne(Math.round((manualDial % 1) * 1000000.0f) / 1000000.0f);
        calculateObjectAngleBaseOne();
        getAngleWrapper.setObjectRotationCount((int) (manualDial * precisionRotation));
        getAngleWrapper.setObjectAngleBaseOne(Math.round((getAngleWrapper.getObjectAngleBaseOne()) * 1000000.0f) / 1000000.0f);
        hgglDialInfo.setGestureRotationCount(getAngleWrapper.getGestureRotationCount());
        hgglDialInfo.setGestureAngleBaseOne(getAngleWrapper.getGestureAngleBaseOne());
        hgglDialInfo.setObjectRotationCount(getAngleWrapper.getObjectRotationCount());
        hgglDialInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());
        onDownAngleObjectCumulativeBaseOne = 0;
        onUpAngleGestureBaseOne = 0;
        doUpDial();

        if(suppressRender == false) {

            requestRender();

        }

    }//End public void doManualGestureDial(final double manualDial)


    public void doManualObjectDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}


	public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection) {

		this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeed = spinEndSpeed;
		this.spinDuration = spinDuration;
		this.lastObjectDirection = objectDirection;
		spinEndTime = System.currentTimeMillis() + spinDuration;
		spinTriggered = true;
		spinTriggeredProgrammatically = true;
		spinAnimationThread = new Thread(this);
		spinAnimationThread.start();

	}//End public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int objectDirection)
    /* Bottom of block convenience methods */


    @Override
    public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView) {

		float[] scratch = new float[16];
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.translateM(rotationMatrix, 0, centerPoint.x + renderOffset[activeTexture][0], centerPoint.y + renderOffset[activeTexture][1], 0f);
		Matrix.scaleM(rotationMatrix, 0, distortDialX, distortDialY, 1f);

		if(HGGLRender.getUseRapidDial() == true) {

			Matrix.rotateM(rotationMatrix, 0, - (float) (multipleDials[activeTexture].getRapidDialAngle() * 360d), 0.0f, 0.0f, 1.0f);
			multipleDials[activeTexture].getRapidDialAngle();

		}
		else if(angleSnapBaseOne == 0) {

			if(multipleDials.length == 1) {

				Matrix.rotateM(rotationMatrix, 0, - (float) (getAngleWrapper.getObjectAngleBaseOne() * 360d), 0.0f, 0.0f, 1.0f);

			}
			else {

				Matrix.rotateM(rotationMatrix, 0, - (float) (((getAngleWrapper.getGestureRotationCount() + getAngleWrapper.getGestureAngleBaseOne()) * multipleDials[activeTexture].getPrecision()) * 360d), 0.0f, 0.0f, 1.0f);

			}//End if(multipleDials.length == 1)

		}
		else if(angleSnapBaseOne != 0) {

			if(multipleDials.length == 1) {

				Matrix.rotateM(rotationMatrix, 0, - (float) (angleSnapNextBaseOne * 360d), 0.0f, 0.0f, 1.0f);

			}
			else /* if(multipleDials.length != 1) */ {

				Matrix.rotateM(rotationMatrix, 0, - (float) ((getAngleWrapper.getGestureRotationCount() + angleSnapNextBaseOne) * multipleDials[activeTexture].getPrecision() * 360d), 0.0f, 0.0f, 1.0f);

			}//End if(multipleDials.length == 1)

		}//End if(HGGLRender.getUseRapidDial() == true)

		Matrix.translateM(rotationMatrix, 0, - centerPoint.x, - centerPoint.y, 0f);
		Matrix.multiplyMM(scratch, 0, matrixProjectionAndView, 0, rotationMatrix, 0);
		square.draw(scratch, activeTexture);

    }//End public void drawFrame(final Square square, final int activeTexture, final Point centerPoint, GL10 unused, final float[] rotationMatrix, final float[] matrixProjectionAndView)


    public void performQuickTap() {

        hgglDialInfo.setQuickTap(true);

        post(new Runnable() {
            @Override
            public void run() {

                ihgRotate.onUp(doUpDial());
                hgglDialInfo.setQuickTap(false);

            }
        });

    }//End public void performQuickTap()



    @Override
    public boolean onTouchEvent(MotionEvent event) {

        sendTouchEvent(this, event);

        return true;

    }


    private OnTouchListener getOnTouchListerField() {

        onTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hgTouchEvent(v, event);

                return true;

            }

        };

        return onTouchListener;

    }//End private OnTouchListener getOnTouchListerField()


    public void sendTouchEvent(View v, MotionEvent event) {

        hgTouchEvent(v, event);

    }


    private void hgTouchEvent(View v, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgglDialInfo.setQuickTap(false);
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap and fling time threshold tolerance
			final long currentTime = System.currentTimeMillis();

			if(currentTime < gestureDownTime + quickTapTime) {

				hgglDialInfo.setQuickTap(true);
				ihgRotate.onUp(doUpDial());
				hgglDialInfo.setQuickTap(false);
				return;

			}//End if(currentTime < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

        if(getPrecisionRotation() != 0) {

			switch(action) {

				case MotionEvent.ACTION_MOVE: {

					setMoveTouch(event);
					ihgRotate.onMove(doMoveDial());

					break;

				}
				case MotionEvent.ACTION_DOWN: {

					setDownTouch(event);
					ihgRotate.onDown(doDownDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_DOWN: {

					setDownTouch(event);
					ihgRotate.onDown(doDownDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_UP: {

					setUpTouch(event);
					ihgRotate.onUp(doUpDial());

					break;

				}
				case MotionEvent.ACTION_UP: {

					setUpTouch(event);
					ihgRotate.onUp(doUpDial());

					break;

				}
				default:

					break;

			}//End switch(action)

        }//End if(getPrecisionRotation() != 0)

    }//End private void hgTouchEvent(View v, MotionEvent event)


	@Override
	public void run() {

		hgglDialInfo.setSpinTriggered(true);

		if(useVariableDial == true) {

			if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0)) {

				//Uses manual start spin speed, end speed and parsed spin duration.
				doSpinAnimationVariablePrecision();

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration != 0) {

				//Uses parsed duration start speed relative to fling and an end speed of 0.
				doDefaultSpinAnimationVariablePrecision();

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration == 0) {

				//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
				post(new Runnable() {
					@Override
					public void run() {

						ihgRotate.onUp(hgglDialInfo);

					}
				});

			}//End if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0))

		}
		else if(useVariableDial == false) {

			if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0)) {

				//Uses manual start spin speed, end speed and parsed spin duration.
				doSpinAnimationFixedPrecision();

				post(new Runnable() {
					@Override
					public void run() {

						setReturnType();
						ihgRotate.onMove(hgglDialInfo);

					}
				});

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration != 0) {

				//Uses parsed duration start speed relative to fling and an end speed of 0.
				doDefaultSpinAnimationFixedPrecision();

				post(new Runnable() {
					@Override
					public void run() {

						setReturnType();
						ihgRotate.onMove(hgglDialInfo);

					}
				});

			}
			else if(spinStartSpeed == 0 && spinEndSpeed == 0 && spinDuration == 0) {

				//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
				post(new Runnable() {
					@Override
					public void run() {

						ihgRotate.onUp(hgglDialInfo);

					}
				});

			}//End if((spinStartSpeed != 0 && spinDuration != 0) || (spinEndSpeed != 0 && spinDuration != 0))

		}//End if(useVariableDial == true)

		hgglDialHandler.sendEmptyMessage(0);

	}//End public void run()


	/* Top of block Fling Functions called on thread */
	private void doSpinAnimationVariablePrecision() {

		//For when spin rate slows down.
		if(spinStartSpeed > spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

				if(spinCurrentSpeed - spinEndSpeed > 0) {

					if(currentTime + 10l < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();
						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);
						getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);
						getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);
						getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
						hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
								if(spinTriggeredProgrammatically == true) {requestRender();}

							}
						});

					}//End if(currentTime + 10l < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

					spinTriggered = false;
					hgglDialInfo.setSpinTriggered(false);
					hgglDialHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed - spinEndSpeed > 0)

			}//End while(flingTriggered == true)

			hgglDialHandler.sendEmptyMessage(0);

		}
		//For when spin rate speeds up.
		else if(spinStartSpeed < spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = spinStartSpeed + (Math.abs(((float) (spinEndTime - currentTime) / spinDuration) - 1) * (spinEndSpeed - spinStartSpeed));

				if(spinCurrentSpeed < spinEndSpeed) {

					//Update view every 10 milliseconds
					if(currentTime + 10l < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();
						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);
						getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);
						getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);
						getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
						hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
								if(spinTriggeredProgrammatically == true) {requestRender();}

							}
						});

					}//End if(currentTime + 10l < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed < spinEndSpeed)) */ {

					spinTriggered = false;
					hgglDialInfo.setSpinTriggered(false);
					hgglDialHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed < spinEndSpeed)

			}//End while(flingTriggered == true)

			hgglDialHandler.sendEmptyMessage(0);

		}//End if(spinStartSpeed > spinEndSpeed)

	}//End private void doSpinAnimationVariablePrecision()


	private void doDefaultSpinAnimationVariablePrecision() {

		long currentTime = System.currentTimeMillis();
		final float spinStartSpeed = ((1000f / (currentTime - gestureDownTime))) * (float) Math.abs(spinStartAngle - fullObjectAngleBaseOne);

		while(spinTriggered == true) {

			final float spinCurrentSpeed = (((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed);

			if(spinCurrentSpeed - spinEndSpeed > 0) {

				if(currentTime + 10l < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();
					fullObjectAngleBaseOne += ((0.01f * spinCurrentSpeed) * lastObjectDirection);
					getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);
					getAngleWrapper.setObjectRotationCount((int) fullObjectAngleBaseOne);
					hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);

					post(new Runnable() {
						@Override
						public void run() {

							if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
							if(spinTriggeredProgrammatically == true) {requestRender();}

						}
					});

				}//End if(currentTime + 10l < System.currentTimeMillis())

			}
			else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

				spinTriggered = false;
				hgglDialInfo.setSpinTriggered(false);
				hgglDialHandler.sendEmptyMessage(0);

			}//End if(spinCurrentSpeed - spinEndSpeed > 0)

		}//End while(flingTriggered == true)

	}//End private void doDefaultSpinAnimationVariablePrecision()


	private void doSpinAnimationFixedPrecision() {

		//For when spin rate slows down.
		if(spinStartSpeed > spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

				if(spinCurrentSpeed - spinEndSpeed > 0) {

					if(currentTime + 10l < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();

						if(precisionRotation > 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

						}
						else if(precisionRotation < 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

						}//End if(precisionRotation > 0)

						getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
						getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
						hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);
						calculateObjectAngleBaseOne();

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
								if(spinTriggeredProgrammatically == true) {requestRender();}

							}
						});

					}//End if(currentTime + 10l < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

					spinTriggered = false;
					hgglDialInfo.setSpinTriggered(false);
					hgglDialHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed - spinEndSpeed > 0)

			}//End while(flingTriggered == true)

			hgglDialHandler.sendEmptyMessage(0);

		}
		//For when spin rate speeds up.
		else if(spinStartSpeed < spinEndSpeed) {

			long currentTime = System.currentTimeMillis();

			while(spinTriggered == true) {

				final float spinCurrentSpeed = spinStartSpeed + (Math.abs(((float) (spinEndTime - currentTime) / spinDuration) - 1) * (spinEndSpeed - spinStartSpeed));

				if(spinCurrentSpeed < spinEndSpeed) {

					//Update view every 10 milliseconds
					if(currentTime + 10l < System.currentTimeMillis()) {

						currentTime = System.currentTimeMillis();

						if(precisionRotation > 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

						}
						else if(precisionRotation < 0) {

							fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

						}//End if(precisionRotation > 0)

						getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
						getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
						hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);
						calculateObjectAngleBaseOne();

						post(new Runnable() {
							@Override
							public void run() {

								if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
								if(spinTriggeredProgrammatically == true) {requestRender();}

							}
						});

					}//End if(currentTime + 10l < System.currentTimeMillis())

				}
				else /* if(!(spinCurrentSpeed < spinEndSpeed)) */ {

					spinTriggered = false;
					hgglDialInfo.setSpinTriggered(false);
					hgglDialHandler.sendEmptyMessage(0);
					return;

				}//End if(spinCurrentSpeed < spinEndSpeed)

			}//End while(flingTriggered == true)

			hgglDialHandler.sendEmptyMessage(0);

		}//End if(spinStartSpeed > spinEndSpeed)

	}//End private void doSpinAnimationFixedPrecision()


	private void doDefaultSpinAnimationFixedPrecision() {

		long currentTime = System.currentTimeMillis();
		final float spinStartSpeed = (float) ((1000f / (currentTime - gestureDownTime)) * Math.abs(spinStartAngle - fullObjectAngleBaseOne));

		while(spinTriggered == true) {

			final float spinCurrentSpeed = ((float) (spinEndTime - currentTime) / spinDuration) * spinStartSpeed;

			if(spinCurrentSpeed - spinEndSpeed > 0) {

				if(currentTime + 10l < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();

					if(precisionRotation > 0) {

						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * lastObjectDirection);

					}
					else if(precisionRotation < 0) {

						fullObjectAngleBaseOne += (0.01f * spinCurrentSpeed * -lastObjectDirection);

					}//End if(precisionRotation > 0)

					getAngleWrapper.setGestureRotationCount((int) fullObjectAngleBaseOne);
					getAngleWrapper.setGestureAngleBaseOne(fullObjectAngleBaseOne % 1);
					hgglDialInfo.setSpinCurrentSpeed(spinCurrentSpeed);
					calculateObjectAngleBaseOne();

					post(new Runnable() {
						@Override
						public void run() {

							if(ihgRotate != null) {ihgRotate.onMove(doMoveDial());}
							if(spinTriggeredProgrammatically == true) {requestRender();}

						}
					});

				}//End if(currentTime + 10l < System.currentTimeMillis())

			}
			else /* if(!(spinCurrentSpeed - spinEndSpeed > 0)) */ {

				spinTriggered = false;
				hgglDialInfo.setSpinTriggered(false);
				hgglDialHandler.sendEmptyMessage(0);

			}//End if(spinCurrentSpeed - spinEndSpeed > 0)

		}//End while(flingTriggered == true)

	}//End private void doDefaultSpinAnimationFixedPrecision()
	/* Bottom of block Fling Functions called on thread */


	private class HGGLDialHandler extends Handler {

		private final WeakReference<HGGLDial> hgglDialWeakReference;

		public HGGLDialHandler(HGGLDial hgglDial) {

			hgglDialWeakReference = new WeakReference<>(hgglDial);

		}

		@Override
		public void handleMessage(Message msg) {

			if(hgglDialWeakReference != null) {

				final HGGLDial hgglDial = hgglDialWeakReference.get();

				if(hgglDial.spinTriggered == false) {

					hgglDial.spinTriggeredProgrammatically = false;
					hgglDial.hgglDialInfo.setSpinTriggered(false);
					hgglDial.hgglDialInfo.setGestureRotationDirection(0);
					hgglDial.hgglDialInfo.setObjectRotationDirection(0);
					hgglDial.hgglDialInfo.setSpinCurrentSpeed(0f);
					hgglDial.ihgRotate.onUp(hgglDialInfo);

					hgglDial.spinAnimationThread = null;

					if(useVariableDial == true) {

						hgglDial.getAngleWrapper.setObjectAngleBaseOne(fullObjectAngleBaseOne);

					}

					hgglDial.post(new Runnable() {
						@Override
						public void run() {

							hgglDial.invalidate();

						}
					});

				}//End if(hgglDial.flingTriggered == true)

			}//End if(hgglDialWeakReference != null)

		}

	}//End private class HGGLDialHandler extends Handler



	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();

		this.ihgRender = this;
		HGGLDial.ihgRotate = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.storedGestureAngleBaseOne = 0d;
		this.storedObjectCount = 0;
		this.currentGestureAngleBaseOne = 0d;
		this.fullObjectAngleBaseOne = 0d;
		this.touchPointerCount = 0;
		this.storedObjectCount = 0;
		this.cumulativeRotate = true;
		this.isSingleFinger = true;
		this.onDownAngleObjectCumulativeBaseOne = 0d;
		this.onUpAngleGestureBaseOne = 0d;
		this.precisionRotation = 1d;
		this.storedPrecisionRotation = 1d;
		this.angleSnapBaseOne = 0d;
		this.angleSnapNextBaseOne = 0d;
		this.angleSnapProximity = 0d;
		this.rotateSnapped = false;
		this.maximumRotation = 0d;
		this.minimumRotation = 0d;
		this.useMinMaxRotation = false;
		this.minMaxRotationOutOfBounds = 0;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.renderOffset = null;
		this.touchPoint.x = 0;
		this.touchPoint.y = 0;
		this.quickTapTime = 75l;
		this.gestureDownTime = 0l;
		this.distortDialX = 1f;
		this.distortDialY = 1f;
		this.suppressRender = false;
		this.variableDialInner = 4.5d;
		this.variableDialOuter = 0.8d;
		this.useVariableDial = false;
		this.useVariableDialCurve = false;
		this.positiveCurve = false;
		HGGLDial.imageRadius = 0d;
		this.imageRadiusX2 = HGGLDial.imageRadius * 2;
		this.originalWidth = 0d;
		this.originalHeight = 0d;
		this.multipleDials = null;

		//Spin Variables
		this.spinStartSpeed = 0f;
		this.spinEndSpeed = 0f;
		this.spinDuration = 0l;
		this.spinEndTime = 0l;
		this.spinStartAngle = 0d;
		this.gestureDownTime = 0l;
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0l;
		this.spinTriggered = false;
		this.spinTriggeredProgrammatically = false;
		this.lastObjectDirection = 0;
		this.spinAnimationThread = null;

	}//End protected void onDetachedFromWindow()

}