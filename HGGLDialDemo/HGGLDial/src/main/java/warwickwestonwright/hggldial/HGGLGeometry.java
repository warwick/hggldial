/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
No Usage a class full of static convenience methods. See descriptions above methods for more details
Note: As the library evolves more methods will be added to this library.
*/

package warwickwestonwright.hggldial;

import android.graphics.Point;

public class HGGLGeometry {

    //Does what it says on the box. Send a centre point and a touch point and the method returns an angle express in a ration of 0 - 1
    //Note: if using this method in other projects, remember that pixels have different heights and widths; meaning the angle will not be entirely accurate.
    public static float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

        float returnVal = 0;

        //+0 - 0.5
        if(touchPoint.x > centerPoint.x) {

            returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

        }
        //+0.5
        else if(touchPoint.x < centerPoint.x) {

            returnVal = (float) (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

        }//End if(touchPoint.x > centerPoint.x)

        return returnVal;

    }//End public static float getAngleFromPoint(final Point centerPoint, final Point touchPoint)

    //Does what it says on the box. Send an angle (as a ratio of 0 - 1) and a radius and it will return a co-ordinate.
    //Note: if using this method in other projects, remember that pixels have different heights and widths; meaning the angle will not be entirely accurate.
    public static Point getPointFromAngle(final double angle, final double radius) {

        final Point coords = new Point();
        coords.x = (int) (radius * Math.sin((angle) * 2 * Math.PI));
        coords.y = (int) -(radius * Math.cos((angle) * 2 * Math.PI));

        return coords;

    }//End public static Point getPointFromAngle(final double angle, final double radius)


    //Does what it says on the box. Send an XY co-ordinate (first two parameters) of the first point and
    // an XY co-ordinate (second two parameters) of the second point and the method returns the diagonal distance between two points.
    public static float getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY) {

        float pinchDistanceX = 0;
        float pinchDistanceY = 0;

        if(firstTouchX > secondTouchX) {

            pinchDistanceX = Math.abs(secondTouchX - firstTouchX);

        }
        else if(firstTouchX < secondTouchX) {

            pinchDistanceX = Math.abs(firstTouchX - secondTouchX);

        }//End if(firstTouchX > secondTouchX)

        if(firstTouchY > secondTouchY) {

            pinchDistanceY = Math.abs(secondTouchY - firstTouchY);

        }
        else if(firstTouchY < secondTouchY) {

            pinchDistanceY = Math.abs(firstTouchY - secondTouchY);

        }//End if(firstTouchY > secondTouchY)

        if(pinchDistanceX == 0 && pinchDistanceY == 0) {

            return 0;

        }
        else {

            pinchDistanceX = (pinchDistanceX * pinchDistanceX);
            pinchDistanceY = (pinchDistanceY * pinchDistanceY);
            return (float) Math.abs(Math.sqrt(pinchDistanceX + pinchDistanceY));

        }//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

    }//End public static float getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)


    //This is a nifty little method. This will return a width and height (as an array of floats) given a diagonal  length (first parameter)
    //and a width and height (last two parameters) used as a width to height ratio. In the return type subscript 0 of the array is the width
    //and 1 the height.
    public static float[] pythagoreanTheorem(final float diagonalLength, final float width, float height) {

        float[] returnVal = {0, 0};

        if(width != 0 && height != 0) {

            if(height > 0) {

                returnVal[1] = (float) Math.sqrt((diagonalLength * diagonalLength) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
                returnVal[0] = (returnVal[1] / (height / width));

            }
            else {

                returnVal[1] = (float) - Math.sqrt((diagonalLength * diagonalLength) / (Math.pow((width / height), 2) + Math.pow((height / height), 2)));
                returnVal[0] = (returnVal[1] / (height / width));

            }//End if(height > 0)

        }
        else if(width != 0 && height == 0) {

            returnVal[0] = width;
            returnVal[1] = 0;
            return returnVal;

        }
        else if(width == 0 && height != 0) {

            returnVal[0] = 0;
            returnVal[1] = height;
            return returnVal;

        }//End if(width != 0 && height != 0)

        return returnVal;

    }//End public static float[] pythagoreanTheorem(final float diagonalLength, final float width, float height)

}